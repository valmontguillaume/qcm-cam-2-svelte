/*
Copyright (c) 2020 Damiano Falcioni
Copyright (c) 2011 Juan Mellado

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

/*
References:
- "ArUco: a minimal library for Augmented Reality applications based on OpenCv"
  http://www.uco.es/investiga/grupos/ava/node/26
- "js-aruco: a port to JavaScript of the ArUco library"
  https://github.com/jcmellado/js-aruco
*/
import CV from './cv.js'

const AR = {}
// mettre un tau à 1 évite les faux positifs
AR.DICTIONARIES = {
  QCMCAM_4X4_157h3: {
    nBits: 16,
    tau: 1,
    codeList: [47103, 18431, 56687, 32679, 62383, 12255, 14847, 59199, 32367, 62175, 44983, 61823, 43647, 46791, 51519, 15987, 27319, 7087, 28431, 36087, 44487, 28055, 55751, 36711, 52647, 23735, 53991, 48159, 30879, 20406, 25727, 50647, 25439, 7895, 41463, 22111, 15303, 31443, 33743, 34173, 42159, 20195, 19317, 13531, 39603, 52779, 3647, 6781, 34427, 46187, 35751, 42227, 7287, 50071, 5551, 3991, 22631, 9527, 34203, 36959, 9171, 18341, 49515, 3279, 12471, 44299, 40131, 36487, 39141, 447, 3569, 10583, 9907, 55311, 9019, 13027, 31365, 51287, 12879, 35733, 18315, 27971, 24935, 34975, 30215, 4587, 42071, 11925, 4767, 21647, 45651, 50759, 57615, 3926, 34723, 19335, 9462, 20903, 27207, 26403, 10119, 9831, 12051, 20695, 17743, 27701, 27537, 29741, 5243, 19547, 6715, 17979, 10543, 19685, 41639, 3915, 10661, 16943, 7431, 25797, 37927, 2171, 5675, 247, 32879, 51719, 8379, 34581, 25251, 27659, 5299, 877, 10343, 4663, 38987, 3853, 2859, 9035, 36163, 8934, 29253, 33373, 13447, 24790, 37091, 25881, 24937, 49447, 23051, 20006, 9611, 34499, 2531, 9585, 33237, 1462, 18995]
  },
  QCMCAM_4X4_90h4: {
    nBits: 16,
    tau: 1,
    codeList: [[251, 187], [254, 218], [103, 223], [118, 175], [254, 71], [158, 207], [95, 151], [111, 229], [187, 124], [183, 171], [247, 77], [121, 205], [207, 86], [46, 235], [45, 63], [126, 27], [182, 91], [191, 200], [211, 213], [233, 93], [158, 46], [249, 145], [14, 183], [108, 94], [204, 213], [52, 111], [87, 178], [240, 203], [4, 255], [13, 246], [147, 122], [159, 161], [202, 31], [197, 55], [249, 37], [238, 42], [53, 117], [181, 50], [15, 154], [51, 45], [84, 158], [196, 242], [38, 62], [221, 130], [172, 228], [104, 103], [97, 233], [217, 88], [138, 173], [153, 70], [17, 167], [42, 15], [70, 101], [134, 139], [176, 43], [148, 113], [165, 84], [24, 117], [28, 90], [56, 178], [75, 100], [85, 104], [93, 65], [107, 18], [139, 162], [133, 156], [156, 137], [201, 98], [204, 152], [199, 160], [36, 177], [8, 174], [23, 24], [50, 140], [36, 232], [80, 46], [81, 148], [132, 108], [133, 42], [188, 4], [33, 35], [68, 21], [9, 41], [42, 40], [80, 19], [97, 36], [131, 68], [102, 0], [104, 1], [128, 160]]
  }
}

AR.Dictionary = function (dicName) {
  this.codes = {}
  this.codeList = []
  this.tau = 0
  this._initialize(dicName)
}

AR.Dictionary.prototype._initialize = function (dicName) {
  this.codes = {}
  this.codeList = []
  this.tau = 0
  this.nBits = 0
  this.markSize = 0
  this.dicName = dicName
  const dictionary = AR.DICTIONARIES[dicName]
  if (!dictionary) { throw 'The dictionary "' + dicName + '" is not recognized.' }

  this.nBits = dictionary.nBits
  this.markSize = Math.sqrt(dictionary.nBits) + 2
  for (let i = 0; i < dictionary.codeList.length; i++) {
    let code = null
    if (typeof dictionary.codeList[i] === 'number') { code = this._hex2bin(dictionary.codeList[i], dictionary.nBits) }
    if (typeof dictionary.codeList[i] === 'string') { code = this._hex2bin(parseInt(dictionary.codeList[i], 16), dictionary.nBits) }
    if (Array.isArray(dictionary.codeList[i])) { code = this._bytes2bin(dictionary.codeList[i], dictionary.nBits) }
    if (code === null) { throw 'Invalid code ' + i + ' in dictionary ' + dicName + ': ' + JSON.stringify(dictionary.codeList[i]) }
    if (code.length !== dictionary.nBits) { throw 'The code ' + i + ' in dictionary ' + dicName + ' is not ' + dictionary.nBits + ' bits long but ' + code.length + ': ' + code }
    this.codeList.push(code)
    this.codes[code] = {
      id: i
    }
  }
  this.tau = dictionary.tau || this._calculateTau()
}

AR.Dictionary.prototype.find = function (bits) {
  let val = ''
  let i; let j
  for (i = 0; i < bits.length; i++) {
    const bitRow = bits[i]
    for (j = 0; j < bitRow.length; j++) {
      val += bitRow[j]
    }
  }
  let minFound = this.codes[val]
  if (minFound) {
    return {
      id: minFound.id,
      distance: 0
    }
  }

  for (i = 0; i < this.codeList.length; i++) {
    const code = this.codeList[i]
    const distance = this._hammingDistance(val, code)
    if (this._hammingDistance(val, code) < this.tau) {
      if (!minFound || minFound.distance > distance) {
        minFound = {
          id: this.codes[code].id,
          distance
        }
      }
    }
  }
  return minFound
}

AR.Dictionary.prototype._hex2bin = function (hex, nBits) {
  return hex.toString(2).padStart(nBits, '0')
}

AR.Dictionary.prototype._bytes2bin = function (byteList, nBits) {
  let bits = ''; let byte
  for (byte of byteList) {
    bits += byte.toString(2).padStart(bits.length + 8 > nBits ? nBits - bits.length : 8, '0')
  }
  return bits
}

AR.Dictionary.prototype._hammingDistance = function (str1, str2) {
  if (str1.length !== str2.length) { throw 'Hamming distance calculation require inputs of the same length' }
  let distance = 0
  let i
  for (i = 0; i < str1.length; i++) {
    if (str1[i] !== str2[i]) { distance += 1 }
  }
  return distance
}

AR.Dictionary.prototype._calculateTau = function () {
  let tau = Number.MAX_VALUE
  for (let i = 0; i < this.codeList.length; i++) {
    for (let j = i + 1; j < this.codeList.length; j++) {
      const distance = this._hammingDistance(this.codeList[i], this.codeList[j])
      tau = distance < tau ? distance : tau
    }
  }
  return tau
}

AR.Dictionary.prototype.generateSVG = function (id) {
  const code = this.codeList[id]
  if (code == null) { throw 'The id "' + id + '" is not valid for the dictionary "' + this.dicName + '". ID must be between 0 and ' + (this.codeList.length - 1) + ' included.' }
  const size = this.markSize - 2
  let svg = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 ' + (size + 4) + ' ' + (size + 4) + '">'
  svg += '<rect x="0" y="0" width="' + (size + 4) + '" height="' + (size + 4) + '" fill="white"/>'
  svg += '<rect x="1" y="1" width="' + (size + 2) + '" height="' + (size + 2) + '" fill="black"/>'
  for (let y = 0; y < size; y++) {
    for (let x = 0; x < size; x++) {
      if (code[y * size + x] === '1') { svg += '<rect x="' + (x + 2) + '" y="' + (y + 2) + '" width="1" height="1" fill="white"/>' }
    }
  }
  svg += '</svg>'
  return svg
}

AR.Dictionary.prototype.generateCANVAS = function (id, dimension) {
  const code = this.codeList[id]
  if (dimension === undefined) dimension = 200
  if (code == null) {
    throw 'The id "' + id + '" is not valid for the dictionary "' + this.dicName + '". ID must be between 0 and ' + (this.codeList.length - 1) + ' included.'
  }
  const size = this.markSize - 2
  const canvas = document.createElement('canvas')
  canvas.width = dimension
  canvas.height = dimension
  const unit = dimension / (size + 4)
  const ctx = canvas.getContext('2d')
  ctx.fillStyle = 'white'
  ctx.fillRect(0, 0, dimension, dimension)
  ctx.fillStyle = 'black'
  ctx.fillRect(unit, unit, unit * (size + 2), unit * (size + 2))
  ctx.fillStyle = 'white'
  for (let y = 0; y < size; y++) {
    for (let x = 0; x < size; x++) {
      if (code[y * size + x] === '1') {
        ctx.fillRect(unit * (x + 2), unit * (y + 2), unit, unit)
      }
    }
  }
  return canvas
}

AR.Marker = function (id, corners, hammingDistance) {
  this.id = id
  this.corners = corners
  this.hammingDistance = hammingDistance
}

AR.Detector = function (config) {
  config = config || {}
  this.grey = new CV.Image()
  this.thres = new CV.Image()
  this.homography = new CV.Image()
  this.binary = []
  this.contours = []
  this.polys = []
  this.candidates = []
  config.dictionaryName = config.dictionaryName || 'QCMCAM_4X4_157h3'
  this.dictionary = new AR.Dictionary(config.dictionaryName)
  this.dictionary.tau = config.maxHammingDistance != null ? config.maxHammingDistance : this.dictionary.tau
}

AR.Detector.prototype.detectImage = function (width, height, data) {
  return this.detect({
    width,
    height,
    data
  })
}

AR.Detector.prototype.detectStreamInit = function (width, height, callback) {
  this.streamConfig = {}
  this.streamConfig.width = width
  this.streamConfig.height = height
  this.streamConfig.imageSize = width * height * 4 // provided image must be a sequence of rgba bytes (4 bytes represent a pixel)
  this.streamConfig.index = 0
  this.streamConfig.imageData = new Uint8ClampedArray(this.streamConfig.imageSize)
  this.streamConfig.callback = callback || function (image, markerList) {}
}

// accept data chunks of different sizes
AR.Detector.prototype.detectStream = function (data) {
  for (let i = 0; i < data.length; i++) {
    this.streamConfig.imageData[this.streamConfig.index] = data[i]
    this.streamConfig.index = (this.streamConfig.index + 1) % this.streamConfig.imageSize
    if (this.streamConfig.index === 0) {
      const image = {
        width: this.streamConfig.width,
        height: this.streamConfig.height,
        data: this.streamConfig.imageData
      }
      const markerList = this.detect(image)
      this.streamConfig.callback(image, markerList)
    }
  }
}

AR.Detector.prototype.detectMJPEGStreamInit = function (width, height, callback, decoderFn) {
  this.mjpeg = {
    decoderFn,
    chunks: [],
    SOI: [0xff, 0xd8],
    EOI: [0xff, 0xd9]
  }
  this.detectStreamInit(width, height, callback)
}

AR.Detector.prototype.detectMJPEGStream = function (chunk) {
  const eoiPos = chunk.findIndex(function (element, index, array) {
    return this.mjpeg.EOI[0] === element && array.length > index + 1 && this.mjpeg.EOI[1] === array[index + 1]
  })
  const soiPos = chunk.findIndex(function (element, index, array) {
    return this.mjpeg.SOI[0] === element && array.length > index + 1 && this.mjpeg.SOI[1] === array[index + 1]
  })

  if (eoiPos === -1) {
    this.mjpeg.chunks.push(chunk)
  } else {
    const part1 = chunk.slice(0, eoiPos + 2)
    if (part1.length) {
      this.mjpeg.chunks.push(part1)
    }
    if (this.mjpeg.chunks.length) {
      const jpegImage = this.mjpeg.chunks.flat()
      const rgba = this.mjpeg.decoderFn(jpegImage)
      this.detectStream(rgba)
    }
    this.mjpeg.chunks = []
  }
  if (soiPos > -1) {
    this.mjpeg.chunks = []
    this.mjpeg.chunks.push(chunk.slice(soiPos))
  }
}

AR.Detector.prototype.detect = function (image) {
  CV.grayscale(image, this.grey)
  CV.adaptiveThreshold(this.grey, this.thres, 2, 7)

  this.contours = CV.findContours(this.thres, this.binary)
  // Scale Fix: https://stackoverflow.com/questions/35936397/marker-detection-on-paper-sheet-using-javascript
  // this.candidates = this.findCandidates(this.contours, image.width * 0.20, 0.05, 10);
//  this.candidates = this.findCandidates(this.contours, image.width * 0.01, 0.05, 10)
  this.candidates = this.findCandidates(this.contours, 12, 0.05, 10)
  this.candidates = this.clockwiseCorners(this.candidates)
  this.candidates = this.notTooNear(this.candidates, 10)

  return this.findMarkers(this.grey, this.candidates, 49)
}

AR.Detector.prototype.findCandidates = function (contours, minSize, epsilon, minLength) {
  const candidates = []
  const len = contours.length
  let contour; let poly; let i

  this.polys = []

  for (i = 0; i < len; ++i) {
    contour = contours[i]

    if (contour.length >= minSize) {
      poly = CV.approxPolyDP(contour, contour.length * epsilon)

      this.polys.push(poly)

      if ((poly.length === 4) && (CV.isContourConvex(poly))) {
        if (CV.minEdgeLength(poly) >= minLength) {
          candidates.push(poly)
        }
      }
    }
  }

  return candidates
}

AR.Detector.prototype.clockwiseCorners = function (candidates) {
  const len = candidates.length
  let dx1; let dx2; let dy1; let dy2; let swap; let i

  for (i = 0; i < len; ++i) {
    dx1 = candidates[i][1].x - candidates[i][0].x
    dy1 = candidates[i][1].y - candidates[i][0].y
    dx2 = candidates[i][2].x - candidates[i][0].x
    dy2 = candidates[i][2].y - candidates[i][0].y

    if ((dx1 * dy2 - dy1 * dx2) < 0) {
      swap = candidates[i][1]
      candidates[i][1] = candidates[i][3]
      candidates[i][3] = swap
    }
  }

  return candidates
}

AR.Detector.prototype.notTooNear = function (candidates, minDist) {
  const notTooNear = []
  const len = candidates.length
  let dist; let dx; let dy; let i; let j; let k

  for (i = 0; i < len; ++i) {
    for (j = i + 1; j < len; ++j) {
      dist = 0

      for (k = 0; k < 4; ++k) {
        dx = candidates[i][k].x - candidates[j][k].x
        dy = candidates[i][k].y - candidates[j][k].y

        dist += dx * dx + dy * dy
      }

      if ((dist / 4) < (minDist * minDist)) {
        if (CV.perimeter(candidates[i]) < CV.perimeter(candidates[j])) {
          candidates[i].tooNear = true
        } else {
          candidates[j].tooNear = true
        }
      }
    }
  }

  for (i = 0; i < len; ++i) {
    if (!candidates[i].tooNear) {
      notTooNear.push(candidates[i])
    }
  }

  return notTooNear
}

AR.Detector.prototype.findMarkers = function (imageSrc, candidates, warpSize) {
  const markers = []
  const len = candidates.length
  let candidate; let marker; let i

  for (i = 0; i < len; ++i) {
    candidate = candidates[i]

    CV.warp(imageSrc, this.homography, candidate, warpSize)

    CV.threshold(this.homography, this.homography, CV.otsu(this.homography))

    marker = this.getMarker(this.homography, candidate)
    if (marker) {
      markers.push(marker)
    }
  }

  return markers
}

AR.Detector.prototype.getMarker = function (imageSrc, candidate) {
  const markSize = this.dictionary.markSize
  const width = (imageSrc.width / markSize) >>> 0
  const minZero = (width * width) >> 1
  const bits = []
  const rotations = []
  let square; let inc; let i; let j

  for (i = 0; i < markSize; ++i) {
    inc = (i === 0 || (markSize - 1) === i) ? 1 : (markSize - 1)

    for (j = 0; j < markSize; j += inc) {
      square = {
        x: j * width,
        y: i * width,
        width,
        height: width
      }
      if (CV.countNonZero(imageSrc, square) > minZero) {
        return null
      }
    }
  }

  for (i = 0; i < markSize - 2; ++i) {
    bits[i] = []

    for (j = 0; j < markSize - 2; ++j) {
      square = {
        x: (j + 1) * width,
        y: (i + 1) * width,
        width,
        height: width
      }

      bits[i][j] = CV.countNonZero(imageSrc, square) > minZero ? 1 : 0
    }
  }

  rotations[0] = bits

  let foundMin = null
  let rot = 0
  for (i = 0; i < 4; i++) {
    const found = this.dictionary.find(rotations[i])
    if (found && (foundMin === null || found.distance < foundMin.distance)) {
      foundMin = found
      rot = i
      if (foundMin.distance === 0) { break }
    }
    rotations[i + 1] = this.rotate(rotations[i])
  }

  if (foundMin) { return new AR.Marker(foundMin.id, this.rotate2(candidate, 4 - rot), foundMin.distance) }

  return null
}

AR.Detector.prototype.rotate = function (src) {
  const dst = []
  const len = src.length
  let i; let j

  for (i = 0; i < len; ++i) {
    dst[i] = []
    for (j = 0; j < src[i].length; ++j) {
      dst[i][j] = src[src[i].length - j - 1][i]
    }
  }

  return dst
}

AR.Detector.prototype.rotate2 = function (src, rotation) {
  const dst = []
  const len = src.length
  let i

  for (i = 0; i < len; ++i) {
    dst[i] = src[(rotation + i) % len]
  }

  return dst
}
export default AR
