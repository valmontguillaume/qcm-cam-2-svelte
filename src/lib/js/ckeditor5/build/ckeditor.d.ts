/**
 * @license Copyright (c) 2003-2024, CKSource Holding sp. z o.o. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */
import { InlineEditor as InlineEditorBase } from '@ckeditor/ckeditor5-editor-inline';
import { Essentials } from '@ckeditor/ckeditor5-essentials';
import { Alignment } from '@ckeditor/ckeditor5-alignment';
import { Autoformat } from '@ckeditor/ckeditor5-autoformat';
import { Bold, Italic, Code } from '@ckeditor/ckeditor5-basic-styles';
import { BlockQuote } from '@ckeditor/ckeditor5-block-quote';
import { Font } from '@ckeditor/ckeditor5-font';
import { Base64UploadAdapter } from '@ckeditor/ckeditor5-upload';
import { Heading } from '@ckeditor/ckeditor5-heading';
import { Image, ImageCaption, ImageUpload, ImageStyle, ImageToolbar, PictureEditing, ImageResize, ImageBlock } from '@ckeditor/ckeditor5-image';
import { Indent } from '@ckeditor/ckeditor5-indent';
import { Link } from '@ckeditor/ckeditor5-link';
import { List } from '@ckeditor/ckeditor5-list';
import { MediaEmbed } from '@ckeditor/ckeditor5-media-embed';
import { Paragraph } from '@ckeditor/ckeditor5-paragraph';
import { PasteFromOffice } from '@ckeditor/ckeditor5-paste-from-office';
import { Table, TableToolbar } from '@ckeditor/ckeditor5-table';
import { TextTransformation } from '@ckeditor/ckeditor5-typing';
import Math from '@isaul32/ckeditor5-math/src/math.js';
import AutoformatMath from '@isaul32/ckeditor5-math/src/autoformatmath.js';
export default class InlineEditor extends InlineEditorBase {
    static builtinPlugins: (typeof TextTransformation | typeof Essentials | typeof Alignment | typeof Paragraph | typeof Heading | typeof Autoformat | typeof Bold | typeof Code | typeof Italic | typeof BlockQuote | typeof Font | typeof Base64UploadAdapter | typeof Image | typeof ImageBlock | typeof ImageCaption | typeof ImageResize | typeof ImageStyle | typeof ImageToolbar | typeof ImageUpload | typeof List | typeof Indent | typeof Link | typeof MediaEmbed | typeof PasteFromOffice | typeof Table | typeof TableToolbar | typeof PictureEditing | typeof Math | typeof AutoformatMath)[];
    static defaultConfig: {
        autoParagraph: boolean;
        toolbar: {
            items: string[];
        };
        image: {
            toolbar: (string | {
                name: string;
                title: string;
                items: string[];
                defaultItem: string;
            })[];
        };
        table: {
            contentToolbar: string[];
        };
        math: {
            engine: string;
            outputType: string;
            className: string;
        };
        language: string;
    };
}
