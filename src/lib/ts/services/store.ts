import { writable } from 'svelte/store'
import Group from '../models/group'
import type { TabId } from '../models/tab'
import QcmList from '../models/qcmsList'
import type Qcm from '../models/qcm'

export const currentTabId = writable<TabId>('home')
export const groups = writable<Group[]>([])
export const currentGroup = writable<Group | undefined>(undefined)
export const qcms = writable<QcmList>(new QcmList())
export const currentQcm = writable<Qcm | undefined>(undefined)
export const currentQcm2 = writable<Qcm | undefined>(undefined)
