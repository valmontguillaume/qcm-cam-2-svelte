import Dexie, { type EntityTable } from 'dexie'
import type Group from '../models/group'

export type Data = {
  uid: number
  data: Group
}

type Student = Data & {
  groupid: string
  studentid: string
}

const db = new Dexie('db_qcmcam2') as Dexie & {
  store_data: EntityTable<Data, 'uid'>,
  store_questions: EntityTable<Data, 'uid'>,
  store_qcms: EntityTable<Data, 'uid'>,
  store_settings: EntityTable<Data, 'uid'>,
  store_sessions: EntityTable<Data, 'uid'>,
  store_students: EntityTable<Data, 'uid'>,
  store_groups: EntityTable<Data, 'uid'>,
  links_groups_students: EntityTable<Student, 'uid'>
}
db.version(4).stores({
  store_data: 'uid,data',
  store_questions: 'uid,data',
  store_qcms: 'uid,data',
  store_settings: 'uid,data',
  store_sessions: '++uid,data',
  store_students: '++uid,data',
  store_groups: '++uid,data',
  links_groups_students: '++uid,groupid,studentid'
})
export const storage = {
  isAvailable (): boolean {
    if (this.db === null) return false
    return true
  },
  db,
  session: {
    get (key: string) {
      const value = sessionStorage.getItem(key)
      if (value !== null) return value
    },
    set (key: string, value: string) {
      sessionStorage.setItem(key, value)
    }
  }
}

export default storage
