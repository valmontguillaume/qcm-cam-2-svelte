import type { TabId } from '../models/tab'
import { currentTabId } from './store'

export function showTab (mouseEvent: MouseEvent, destinationTab: TabId): void {
  if (!isRegularClick(mouseEvent)) {
    return // to allow right clicks and opening in new tabs
  }
  mouseEvent.preventDefault()
  currentTabId.set(destinationTab)
  window.history.pushState({}, '', `?tab=${destinationTab}`)
}

export function isRegularClick (mouseEvent: MouseEvent): boolean {
  return mouseEvent.button === 0 && !mouseEvent.ctrlKey && !mouseEvent.metaKey
}

export function showModal (modalId: string): void {
  const modal = document.getElementById(`qm-${modalId}`)
  if (modal === null) {
    console.error(`Modal with id qm-${modalId} not found`)
    return
  }
  modal.classList.add('is-active')
}

export function hideModal (modal: HTMLElement): void {
  modal.classList.remove('is-active')
}
