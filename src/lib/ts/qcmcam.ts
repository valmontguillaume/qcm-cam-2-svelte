import display from './display'
import { storage } from './services/storage'
import utils from './services/utils'
import Group from './models/group'
import language from './services/language'
import QcmsList from './models/qcmsList'
import Qcm from './models/qcm'
import slider from './services/slider'
import { groups, qcms } from './services/store'
import { get } from 'svelte/store'
import { tabIdValidKeys } from './models/tab'
// import detection from './services/markerdetection.js'

/* TODO :
  [] réfléchir à l'utilité du bouton save (on peut enregistrer automatiquement)
  [] permettre de télécharger en un seul fichier QCMs et Utilisateurs
  [] permettre de faire des catégories dans le menu des QCM
  ...
*/

language.getNavigatorLanguage()

if (storage.isAvailable()) {
  storage.db.store_groups.toArray()
    .then(async (result: Array<{ uid: number, data: Group }>) => {
      const newGroups: Group[] = get(groups)
      result.forEach((element: { uid: number, data: Group }) => {
        Group.remake(element.data, element.uid).then((group: Group) => {
          newGroups.push(group)
        }).catch(() => { console.log('Pb de chargement des groupes', element.uid) })
      })
      groups.set(newGroups)
    })
    .catch(() => {
      console.log('Pb de chargement des groupes')
    })

  storage.db.store_data.get('QCMCamQcms')
    .then((item: { data: QcmsList }) => {
      get(qcms).list = QcmsList.remake(item.data)
    })
    .catch(() => {
      console.log('Pas de Qcms')
    })

  // check if all QCM are in the list
  storage.db.store_qcms.each((item: { data: Group }) => {
    // create qcm ids list from qcms.list
    const qcmIds = get(qcms).list.map((qcm) => { return qcm[0] })
    let nomoreQcms = true
    if (!qcmIds.includes(item.data.id)) {
      try {
        get(qcms).list = [...get(qcms).list, [item.data._id, item.data._name, item.data._questionsList[0], item.data._questionsList.length]]
        nomoreQcms = false
      } catch (err) {
        console.warn(err)
      }
    }
    if (!nomoreQcms) get(qcms).save()
  })

  storage.db.store_settings.get('config')
    .then((item: { data: any }) => {
      display.restoreSettings(item.data)
    })
    .catch(() => {
      console.log('Pas de config dans la base')
    })
}
window.onresize = () => { display.setQuestionFontSize(); display.setQCMQuestionFontSize() }

/**
 * Comportement des boutons de la page d'accueil
 */
for (const i of tabIdValidKeys) {
  const elem = document.getElementById('btn-' + i)
  if (elem !== null) {
    elem.onclick = () => {
      if (i === 'qcm') {
        display.QCMList(get(qcms))
      } else if (i === 'openusage') {
        slider.checkUsageButtons()
        display.cameraInterface(slider.params())
      } else if (i === 'statistics') {
        display.statisticsDisplay(get(groups))
      }
    }
  }
}
utils.DOM.addAction('newuserToCreate', 'click', () => { display.showTab('qm-newuser') })
utils.DOM.addAction('close-show', 'click', () => { display.hideQCMCamShow() })
const importQCMFile = document.getElementById('import-qcm-file')
if (importQCMFile !== null) importQCMFile.onchange = (event) => { openQCMFile(get(qcms), event) }
const importQCMUrl = document.getElementById('import-qcm-url')
if (importQCMUrl !== null) importQCMUrl.oninput = (event) => { importOnlineFile(get(qcms), event) }
const importQCMText = document.getElementById('import-qcm-text')
if (importQCMText !== null) importQCMText.oninput = (event) => { importQCMFromText(get(qcms), event) }
const switchShowCorrectAnswers = utils.DOM.getById('switchShowCorrectAnswers') as HTMLInputElement
if (switchShowCorrectAnswers !== null) {
  switchShowCorrectAnswers.checked = false
  switchShowCorrectAnswers.onclick = () => { utils.DOM.toggleClass('questions-list', 'showCorrectAnswers') }
}
const switchIsQCMColored = utils.DOM.getById('switchIsQCMColored') as HTMLInputElement
if (switchIsQCMColored !== null) {
  switchIsQCMColored.onclick = () => {
    display.switchIsQCMColored(switchIsQCMColored.checked)
  }
}
utils.DOM.addAction('defaultColor0', 'click', () => { display.resetDefaultColors() })
for (let i = 1; i < 5; i++) {
  const colorsetter = utils.DOM.getById('defaultColor' + String(i)) as HTMLInputElement
  if (colorsetter) colorsetter.onchange = () => { display.setDefaultColor(i, colorsetter.value) }
}
utils.DOM.addAction('defaultColorEditor0', 'click', () => { display.setQCMDefaultColors() })
for (let i = 1; i < 5; i++) {
  const colorsetter = utils.DOM.getById('defaultColorEditor' + String(i)) as HTMLInputElement
  if (colorsetter) colorsetter.onchange = () => { display.setQCMAnswerColor(i, colorsetter.value) }
}

utils.DOM.addAction('btn-add-qcm', 'click', () => { const qcm = new Qcm('Titre', 'qcmcam2'); const question = qcm.createQuestion('<h3>Question 1</h3>'); question.addAnwser(''); question.addAnwser(''); question.save(); get(qcms).list.push([qcm.id, qcm.name, qcm.questionsList[0], 1]); display.QCMQuestionsList(qcm.name, qcm, get(qcms), get(qcms).list.length - 1) })
utils.DOM.addAction('btn-download-qcms', 'click', () => { get(qcms).export().then(blob => { utils.file.download(blob, 'qcmcam-liste-des-qcm.txt') }).catch(err => { console.log(err) }) })
utils.DOM.addAction('btn-qcm-delete', 'click', () => { display.QCMDelete(get(qcms)) })

utils.DOM.addAction('btn-download-groups', 'click', () => { utils.file.download(new Blob([JSON.stringify(get(groups), null, 2)], { type: 'application/json' }), 'groupes.txt') })
const importGroupFile = document.getElementById('import-groups-file')
if (importGroupFile !== null) importGroupFile.onchange = (event) => { openGroupsFile(event) }
const importGroupText = utils.DOM.getById('import-groups-text')
if (importGroupText !== null) importGroupText.oninput = (event: Event) => { if (event.target instanceof HTMLInputElement && importGroupText !== null) display.GroupImportString(event.target.value, get(groups)) }

const closeModalElements = document.querySelectorAll<HTMLElement>('.closebutton, .modal-background')
for (const $close of Array.from(closeModalElements)) {
  const $target = $close.closest('.modal')
  if ($target !== null) $close.onclick = () => { display.hideModal($target) }
}
// Fenêtre de configuration du diaporama
['commands', 'commands0', 'commands1'].forEach(id => {
  const commands = document.getElementById('qcm-' + id)
  if (commands !== null) {
    commands.onclick = (event) => {
      if (event.target !== null) {
        let target: Element | null = event.target as Element
        while (target !== null && target.nodeName !== 'BUTTON') {
          target = target.parentNode as Element
        }
        if (target !== null) {
          const command = target.id
          launchCommand(command)
        }
      }
    }
  }
})

function launchCommand (command: string): void {
  let zero = ''
  if (command.indexOf('0') > 0) zero = '0'
  if (command.indexOf('hide') > 0) {
    display.slideAddCache(zero)
  } else if (command.indexOf('next') > 0) {
    display.nextQuestion(zero)
  } else if (command.indexOf('goodAnswer') > 0) {
    display.showGoodAnswer(zero)
  } else if (command.indexOf('stop') > 0) {
    document.querySelectorAll('btn-')
    if (display.stopDetection(zero)) {
      if (zero === '') {
        document.querySelectorAll('#qcm-commands .ri-stop-large-line, #qcm-commands1 .ri-stop-large-line').forEach(el => { el.classList.remove('ri-stop-large-line'); el.classList.add('ri-play-large-line') })
      } else {
        document.querySelectorAll('#qcm-commands0 .ri-stop-large-line').forEach(el => { el.classList.remove('ri-stop-large-line'); el.classList.add('ri-play-large-line') })
      }
    } else {
      if (zero === '') {
        document.querySelectorAll('#qcm-commands .ri-play-large-line, #qcm-commands1 .ri-play-large-line').forEach(el => { el.classList.remove('ri-play-large-line'); el.classList.add('ri-stop-large-line') })
      } else {
        document.querySelectorAll('#qcm-commands0 .ri-play-large-line').forEach(el => { el.classList.remove('ri-play-large-line'); el.classList.add('ri-stop-large-line') })
      }
    }
  } else if (command.indexOf('stats') > 0) {
    display.displayStatsOfAnswers(zero)
  }
}

utils.DOM.addAction('qcm-mode-choice0', 'click', () => { display.setQCMParameter('normal') })
utils.DOM.addAction('qcm-mode-choice1', 'click', () => { display.setQCMParameter('double') })
utils.DOM.addAction('qcm-mode-choice2', 'click', () => { display.setQCMParameter('duel') })

utils.DOM.addAction('btn-maker-callroll', 'click', () => { display.createSubgroupsByVote() })
utils.DOM.addAction('gp-maker-cancel', 'click', () => { display.hideModal(utils.DOM.getById('qm-gp-maker')) })
utils.DOM.addAction('gp-maker-confirm', 'click', () => { display.confirmSubGroups().catch(err => { console.log(err) }); display.hideModal(utils.DOM.getById('qm-gp-maker')) })

utils.DOM.addAction('btn-refresh-camera', 'click', () => { display.refreshCamerasList().catch(err => { console.log(err) }) })
utils.DOM.addAction('btn-start-qcm', 'click', () => { display.startQCM(get(groups)).catch(err => { console.log(err) }) })
utils.DOM.addAction('btn-start-qcm-after-callroll', 'click', () => { utils.DOM.removeClass('qm-callroll', 'is-active'); display.startQCM(get(groups), true).catch(err => { console.log(err) }) })

utils.DOM.addAction('btn-tab-form', 'click', () => { display.showQuestionTab('form') })
utils.DOM.addAction('btn-tab-markdown', 'click', () => { display.showQuestionTab('markdown') })

utils.DOM.addAction('btn-showNames', 'click', () => { display.showNamesOfStudents() })
utils.DOM.addAction('btn-showNames0', 'click', () => { display.showNamesOfStudents('0') })
utils.DOM.addAction('btn-showUserAnswers', 'click', () => { display.showAnswersOfStudents() })
utils.DOM.addAction('btn-showUserAnswers0', 'click', () => { display.showAnswersOfStudents('0') })

// réglages
utils.DOM.addAction('btn-showGoodAnswers', 'click', () => { display.showGoodAnswersOfStudents() })

utils.DOM.addAction('changeopinionyes', 'click', () => { display.changeOpinion('yes') })
utils.DOM.addAction('changeopinionno', 'click', () => { display.changeOpinion('no') })

utils.DOM.addAction('stopscanyes', 'click', () => { display.changeAutoStopOption('yes') })
utils.DOM.addAction('stopscanno', 'click', () => { display.changeAutoStopOption('no') })
utils.DOM.addAction('defaultTime', 'input', () => {
  const input = document.getElementById('defaultTime') as HTMLInputElement
  display.setDefaultSlideTime(input.value)
})

function openGroupsFile (event: Event): void {
  // le fichier est un txt avec lignes au format Numero\tNom[\tclasse] ou Nom seul
  if (event.target !== null) {
    const input = event.target as unknown as HTMLInputElement
    const reader = new FileReader()
    reader.onload = function () {
      if (typeof reader.result === 'string') {
        const result = Group.import(reader.result, get(groups))
        if (result[1]) {
          groups.set(result[0])
          display.GroupList(get(groups))
        } else if (result[0] !== '') {
          display.GroupImportString(result[0], get(groups))
        } else {
          display.hideModal(document.getElementById('qm-import-groups') as Element)
        }
      }
      //
    }
    if (input.files !== null) {
      const file = input.files[0]
      reader.readAsText(file)
    }
  }
}
function openQCMFile (qcms: QcmsList, event: Event): void {
  if (event.target !== null) {
    const importModalFoot = document.querySelector('#qcm-import-qcm .has-text-danger')
    const input = event.target as unknown as HTMLInputElement
    const reader = new FileReader()
    reader.onload = function () {
      if (typeof reader.result === 'string') {
        if (input.files !== null) {
          const result = JSON.parse(reader.result)
          if (result.uid !== undefined) {
            if (qcms.importQcmList(reader.result, input.files[0].name)) {
              utils.DOM.removeClass('qm-import-qcm', 'is-active')
            } else {
              (importModalFoot as HTMLElement).innerText = 'Erreur d\'import'
            }
          } else {
            const qcm = qcms.importQcm(reader.result, input.files[0].name)
            if (qcm !== undefined) {
              display.QCMList(qcms)
              display.QCMQuestionsList(qcm.name, qcm, qcms, qcms.list.length - 1)
              qcms.save()
              utils.DOM.removeClass('qm-import-qcm', 'is-active')
            } else if (importModalFoot !== null) {
              (importModalFoot as HTMLElement).innerText = 'Erreur de format de fichier'
            }
          }
        }
      }
    }
    reader.onerror = function () {
      if (importModalFoot !== null) {
        (importModalFoot as HTMLElement).innerText = 'Erreur de chargement'
      }
      return false
    }
    if (input.files !== null) {
      const file = input.files[0]
      reader.readAsText(file)
    }
  }
}
function importOnlineFile (qcms: QcmsList, event: Event): void {
  if (event.target !== null) {
    const url = (event.target as HTMLInputElement).value
    let testurl
    // analyse de la validité de l'url
    try {
      testurl = new URL(url)
    } catch (e) {
      return
    }
    const importModalFoot = document.querySelector('#qcm-import-qcm .has-text-danger')
    const pathName = testurl.pathname
    const fileName = pathName.split('/').pop()
    let nomDuFichier = window.prompt('Donnez un nom au QCM :', fileName)
    if (nomDuFichier === null) {
      nomDuFichier = 'QCM importé'
    }
    const reader = new XMLHttpRequest()
    reader.open('GET', 'phploader.php?url=' + encodeURIComponent(url), false)
    reader.onreadystatechange = () => {
      if (reader.readyState === 4) {
        if (reader.status === 200 || reader.status === 0) {
          try {
            const qcm = qcms.importQcm(reader.responseText, nomDuFichier)
            if (qcm !== undefined) {
              display.QCMList(qcms)
              display.QCMQuestionsList(qcm.name, qcm, qcms, qcms.list.length - 1)
              qcms.save()
              utils.DOM.removeClass('qm-import-qcm', 'is-active')
            } else if (importModalFoot !== null) {
              (importModalFoot as HTMLElement).innerText = 'Erreur de format de fichier'
            }
          } catch (e) {
            console.log(e)
          }
        }
      }
    }
    reader.send(null)
  }
}
function importQCMFromText (qcms: QcmsList, event: Event): void {
  if (event.target !== null) {
    const importModalFoot = document.querySelector('#qcm-import-qcm .has-text-danger')
    const input = event.target as HTMLInputElement
    if (input.value !== '') {
      let Titre = window.prompt('Donnez un nom au QCM :')
      if (Titre === null) {
        Titre = 'QCM importé'
      }
      const qcm = qcms.importQcm(input.value, Titre)
      if (qcm !== undefined) {
        display.QCMList(qcms)
        display.QCMQuestionsList(qcm.name, qcm, qcms, qcms.list.length - 1)
        qcms.save()
        utils.DOM.removeClass('qm-import-qcm', 'is-active')
      } else if (importModalFoot !== null) {
        (importModalFoot as HTMLElement).innerText = 'Erreur de format de fichier'
      }
    }
  }
}
