export const DEFAULT_TAB = 'home'

export const tabIdValidKeys = <const>[DEFAULT_TAB, 'start', 'markers', 'openusage', 'qcm', 'groups', 'statistics', 'diaporama-settings', 'QCMCamShow']

type TabIdValidKeysType = typeof tabIdValidKeys
export type TabId = TabIdValidKeysType[number]

export function isTabId (obj: unknown): obj is TabId {
  if (obj == null || typeof obj !== 'string') return false
  return tabIdValidKeys.includes(obj as TabId)
}

export function isTabIds (obj: unknown[]): obj is TabId[] {
  if (obj == null || !Array.isArray(obj)) return false
  return obj.every(isTabId)
}
