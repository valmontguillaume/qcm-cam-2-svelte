import storage from '../services/storage.js'

// permet de garder une trace de la configuration d'une session,
// pour lecture a posteriorie et sauvegarde.
export default class Session {
  private readonly _date: number // date de création
  private _id: number // identifiant dans la base
  private readonly _mode: string // mode = normal, double, duel
  private readonly _qcms: string[][] // [[qcmId0, qcmId1, ...], [qcmId0, qcmId1, ...]]
  // permet de récupérer les paramétrages des qcms
  private readonly _shuffleAnswersMode: boolean[] // mélange de l'ordre des réponses si autorisé ou pas
  private readonly _shuffleQuestionsMode: boolean[] // mélange de l'ordre des questions
  private readonly _questions: questionConf[][] // à compléter si extraits des qcms ou params aléatoires,[[{questionId0: ['A','D', ...]]}, {questionId1: ['B','C',...]}, ...], [[{questionId0: ['A','C', ...]}, {questionId1: ['B','A', ...]}, ...]]
  // si pas de mélange, ['A','B', ...] ou rien
  // permet de récupérer l'ordre des questions et l'ordre des réponses de chaque qcm
  private readonly _groups: number[][] // [[gpid1, gpid2, ...], et si extraits : [student0, student1, ...], [student0, student1, ...]]
  private readonly _results: resultRecord[][]

  constructor (mode: string, qcms: string[][], questions: questionConf[][], groups: number[][], shuffleAnswersMode: boolean[], shuffleQuestionsMode: boolean[]) {
    this._date = Date.now()
    this._id = 0
    this._mode = mode
    this._qcms = []
    this._questions = [...questions]
    this._groups = [...groups]
    this._shuffleAnswersMode = shuffleAnswersMode
    this._shuffleQuestionsMode = shuffleQuestionsMode
    this._results = []
    this.save()
  }

  addRecord (qId: number, stId: number, ans: string, cor: boolean): void {
    this._results[qId].push({ qId, stId, ans, cor })
  }

  getId (): number {
    return Math.floor(Math.random() * 10000)
  }

  save (): void {
    if (storage.isAvailable()) {
      const dataToSend: dataToSend = { data: this }
      if (this._id !== 0) dataToSend.uid = this._id
      storage.db.store_sessions.put(dataToSend).then((id: number) => { this._id = id }).catch(() => { console.error('Erreur de sauvegarde de la session ') })
    }
  }

  async load (id: number): Promise<Session> {
    if (storage.isAvailable()) {
      const result = await storage.db.store_sessions.get(id)
      if (result !== undefined) {
        return Session.remake(result.data)
      } else return new Session('normal', [], [], [], [false], [false])
    } else return new Session('normal', [], [], [], [false], [false])
  }

  static remake (data: any): Session {
    return new Session(data.mode, data.qcms, data.questions, data.groups, data.shuffleAnswersMode, data.shuffleQuestionsMode)
  }

  getSession (): any {
    return {
      date: this._date,
      id: this._id,
      mode: this._mode,
      qcms: this._qcms,
      questions: this._questions,
      groups: this._groups,
      shuffleAnswersMode: this._shuffleAnswersMode,
      shuffleQuestionsMode: this._shuffleQuestionsMode,
      results: this._results
    }
  }

  static copySession (session: Session): Session {
    return new Session(session.mode, session.qcms, session.questions, session.groups, session.shuffleAnswersMode, session.shuffleQuestionsMode)
  }

  get shuffleAnswersMode (): boolean {
    return this._shuffleAnswersMode
  }

  get shuffleQuestionsMode (): boolean {
    return this._shuffleQuestionsMode
  }

  get questions (): questionConf[][] {
    return this._questions
  }

  get groups (): string[][] {
    return this._groups
  }

  get qcms (): string[][] {
    return this._qcms
  }

  get results (): resultRecord[][] {
    return this._results
  }

  get id (): number {
    return this._id
  }

  get date (): number {
    return this._date
  }

  get mode (): string {
    return this._mode
  }
}

/**
 * qId : question id
 * stId : student id
 * ans : answer
 * cor : correct
 * */
interface resultRecord {
  qId: number
  stId: number
  ans: string
  cor: boolean
}

interface dataToSend {
  data: Session
  uid?: number
}
/**
 * qId : question id
 * questions : array of [answer id, letter]
 */

interface questionConf {
  qId: number
  questions: string[]
}
