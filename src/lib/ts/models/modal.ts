export const modalIdValidKeys = <const>['import-groups', 'connect', 'newuser', 'import-qcm', 'settings', 'gp-maker']

type ModalIdValidKeysType = typeof modalIdValidKeys
export type ModalId = ModalIdValidKeysType[number]

export function isModalId (obj: unknown): obj is ModalId {
  if (obj == null || typeof obj !== 'string') return false
  return modalIdValidKeys.includes(obj as ModalId)
}

export function isModalIds (obj: unknown[]): obj is ModalId[] {
  if (obj == null || !Array.isArray(obj)) return false
  return obj.every(isModalId)
}
