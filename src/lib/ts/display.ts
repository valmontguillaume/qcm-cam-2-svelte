import utils from './services/utils'
// import language from './services/language'
import Student from './models/student'
import Group from './models/group'
import Qcm from './models/qcm'
import type QcmList from './models/qcmsList'
import Question from './models/question'
// import type Answer from './models/answer'
import storage from './services/storage'
import camera from './services/camera'
import detection from './services/markerdetection'
import Votes from './models/votes'
import Chart from '../js/chartjs/Chart'
import katex from '../js/katex/katex.mjs'
import language from './services/language'
import { currentGroup, currentQcm, currentQcm2 } from './services/store'
import { get } from 'svelte/store'
import { showModal } from './services/navigation'
import { currentTabId } from '../ts/services/store'
import Session from './models/session'

interface Config {
  changeOpinion: boolean
  scanAutoStop: boolean
  defaultSlideTime: number
  startMessage: string
  language: string
  defaultColors: string[]
}
interface Display {
  inlineEditors: any[]
  restoreSettings: (arg0: Config) => void
  saveSettings: () => void
  changeOpinion: (arg0: string) => void
  changeAutoStopOption: (arg0: string) => void
  setLanguage: (arg0: string) => void
  setDefaultSlideTime: (arg0: string) => void
  setQCMParameter: (arg0: string) => void
  resetDefaultColors: () => void
  setDefaultColor: (index: number, colorValue: string) => void
  updateColors: () => void
  updateQCMColors: () => void
  setQCMDefaultColors: () => void
  setQCMAnswerColor: (index: number, colorValue: string) => void
  setStyleSheetColor: (arg0?: string) => void
  refreshCamerasList: (arg0?: HTMLSelectElement) => Promise<void>
  showQuestionTab: (arg0: string) => void
  showModal: (arg0: string) => void
  hideModal: (arg0: Element | null) => void
  GroupList: (arg0: Group[]) => void
  GroupImportString: (arg0: string, groups: Group[]) => Promise<void>
  GroupStudentsList: (arg0: number, arg1: Group[]) => void
  createStudentCard: (arg0: Student) => HTMLElement
  displaySubgroups: (group: Group, $containerA: HTMLElement, $containerB: HTMLElement) => void
  createSubgroupsByVote: () => void
  startVoteForSubgroup: (group: Group) => void
  confirmSubGroups: () => Promise<void>
  unActiveGroupList: () => void
  editNewStudent: (arg0: HTMLElement, arg1: Group, arg2: Group[]) => Promise<void>
  createSelect: (arg0: number[], arg1: number) => any
  createGroupCheckList: (arg0: Student, arg1: Group[]) => any
  displayQuestionEditor: (arg0: Question, arg1: number, arg2: Qcm, arg3: QcmList, arg4: string) => Promise<HTMLElement>
  refreshQuestions: (arg0: Qcm, arg1: QcmList, arg2: string) => void
  renderLatex: (arg0: string) => void
  QCMList: (arg0: QcmList) => void
  unActiveQCMList: () => void
  QCMQuestionsList: (arg0: string, arg1: Qcm, arg2: QcmList, arg3: number) => void
  emptyQCMQuestionsList: () => void
  QCMDelete: (arg0: QcmList) => void
  switchIsQCMColored: (arg0: boolean) => void
  resetGroupStudentsList: () => void
  cameraInterface: (arg0: { firstSlide: string, numberOfQuestions: number, camera: string, mode: string }) => void
  slideAddCache: (arg0: string) => void
  slidePopulate: (arg0: Question[], arg1?: string, arg2?: boolean, arg3?: boolean) => void
  createQcmCard: (arg0: QcmList, arg1: number, arg2: Question) => any
  setQuestionFontSize: () => void
  setQCMQuestionFontSize: () => void
  setQCMCardsFontSize: () => void
  slideGroupPopulate: (arg0: Group, arg1?: string) => void
  callRoll: (arg0: Group, arg1?: Group) => void
  startCallRoll: (arg0: Group, arg1?: Group) => void
  stopCallRoll: () => void
  callRollSetPresentState: (arg0: Group, arg1?: Group) => void
  callRollPopulate: (arg0: Group) => void
  startQCM: (arg0: Group[], arg1?: boolean) => Promise<void>
  hideQCMCamShow: () => void
  slideMarkerSetAnswerState: () => void
  nextQuestion: (arg0?: string) => void
  startDetection: (arg0?: string) => void
  startCountDown: (arg0?: string) => void
  stopDetection: (arg0?: string) => boolean
  showGoodAnswer: (arg0?: string) => void
  slideMarkersReset: (arg0?: string) => void
  statisticsDisplay: (arg0: Group[]) => void
  questionStatistics: (index: number, container: HTMLElement, question?: Question) => void
  groupStatistics: (container: HTMLElement, group: Group, votes: Votes, questions: Question[]) => void
  answersStatistics: (votes: Votes, index: number, targetDiv: HTMLElement) => void
  appendStatBar: (li: HTMLElement, color: string, percent: number) => void
  studentStatistics: (student: Student, votes: Votes, questions: Question[]) => HTMLElement
  showGoodAnswersOfStudents: () => void
  showAnswersOfStudents: (second?: string) => void
  showNamesOfStudents: (arg0?: string) => void
  displayStatsOfAnswers: (arg0?: string) => void
  showQRCode: (peerId: string) => void
  peerConnected: () => void
  connectedMessage: () => void
}

let currentQuestionIndex: number = 1
let currentQuestionIndex2: number = 1
let currentQuestions: Question[]
let currentQuestions2: Question[]
let votes: Votes
let votes2: Votes
let dectectionWithCamera = false
let questionToCopy = ''
let questionToCut = { question: '', QCMfrom: '', index: 0 }
let session: Session

const initialColors = ['#a8f9f9', '#f9b7d8', '#c5bcf8', '#f8f596']
const config: Config = {
  changeOpinion: true,
  scanAutoStop: true,
  startMessage: '',
  language: 'fr',
  defaultSlideTime: 15,
  defaultColors: initialColors
}
// let goodAnswers: string[] = []
// let qcmChoice: number[] = []
const textToQuestionWidthFactor = 27.302
let tictoc: number | null = null
const display: Display = {
  inlineEditors: [],
  restoreSettings: function (settings) {
    if (settings.changeOpinion !== undefined) {
      config.changeOpinion = settings.changeOpinion
    }
    if (settings.scanAutoStop !== undefined) {
      config.scanAutoStop = settings.scanAutoStop
    }
    if (settings.startMessage !== undefined) {
      config.startMessage = settings.startMessage
    }
    if (settings.language !== undefined) {
      config.language = settings.language
      this.setLanguage(settings.language)
    }
    if (settings.defaultColors !== undefined) {
      config.defaultColors = settings.defaultColors
      this.updateColors()
    }
  },
  saveSettings: function () {
    if (storage.isAvailable()) {
      storage.db.store_settings.put({ uid: 'config', data: config })
    }
  },
  cameraInterface (diapo: { firstSlide?: string, numberOfQuestions?: number, camera: any, mode?: string }) {
    if (diapo.camera === 'local') {
      utils.DOM.removeClass('videotest', 'is-hidden')
    }
  },
  changeOpinion: function (opinionOption) {
    config.changeOpinion = opinionOption === 'yes'
    this.saveSettings()
  },
  changeAutoStopOption: function (autoStopOption) {
    config.scanAutoStop = autoStopOption === 'yes'
    this.saveSettings()
  },
  setLanguage: function (lang) {
    config.language = lang
    // refresh page
    language.setLanguage(lang)
    this.saveSettings()
  },
  setDefaultSlideTime: function (time) {
    config.defaultSlideTime = Number(time)
    this.saveSettings()
  },
  showQuestionTab: function (id: string) {
    let other = 'form'
    if (id === 'form') other = 'markdown'
    utils.DOM.addClass('modal-tab-' + other, 'is-hidden')
    utils.DOM.removeClass('btn-tab-' + other, 'is-active')
    utils.DOM.addClass('btn-tab-' + id, 'is-active')
    utils.DOM.removeClass('modal-tab-' + id, 'is-hidden')
  },
  hideModal: function (domEl: Element | null) {
    if (domEl !== null) {
      domEl.classList.remove('is-active')
      if (domEl.id === 'qm-diaporama-selection') {
        this.hideQCMCamShow()
      }
    }
  },
  setQCMParameter: function (mode) {
    if (mode === 'normal') {
      utils.DOM.addClass('qcm-choice2', 'is-hidden')
      utils.DOM.addClass('qcm-questions-choice2', 'is-hidden')
    } else if (mode === 'double') {
      utils.DOM.removeClass('qcm-choice2', 'is-hidden')
      utils.DOM.removeClass('qcm-questions-choice2', 'is-hidden')
    } else if (mode === 'duel') {
      utils.DOM.addClass('qcm-choice2', 'is-hidden')
      utils.DOM.addClass('qcm-questions-choice2', 'is-hidden')
    }
  },
  resetDefaultColors () {
    config.defaultColors = initialColors
    this.saveSettings()
    this.updateColors()
  },
  setDefaultColor (index, colorValue) {
    config.defaultColors[index - 1] = colorValue
    this.updateColors()
    this.saveSettings()
  },
  updateColors () {
    for (let i = 1; i < 5; i++) {
      const colorInput = utils.DOM.getById('defaultColor' + String(i)) as HTMLInputElement
      colorInput.value = config.defaultColors[i - 1]
      const colorInfo = utils.DOM.getById('editorConfigColor' + String(i)) as HTMLSpanElement
      colorInfo.style.backgroundColor = config.defaultColors[i - 1]
    }
  },
  updateQCMColors () {
    const qcm = get(currentQcm)
    if (qcm === undefined) return
    for (let i = 1; i < 5; i++) {
      const colorInput = utils.DOM.getById('defaultColorEditor' + String(i)) as HTMLInputElement
      if (qcm.colors[i - 1] !== undefined && qcm.colors[i - 1] !== '') colorInput.value = qcm.colors[i - 1]
      else colorInput.value = config.defaultColors[i - 1]
    }
    this.setStyleSheetColor()
  },
  setQCMDefaultColors () {
    const qcm = get(currentQcm)
    if (qcm !== undefined) {
      qcm.colors = config.defaultColors
      this.updateQCMColors()
      qcm.save()
    }
  },
  setQCMAnswerColor (index, colorValue) {
    const qcm = get(currentQcm)
    if (qcm !== undefined) {
      qcm.colors[index - 1] = colorValue
      this.setStyleSheetColor()
      qcm.save()
    }
  },
  setStyleSheetColor (second = '') {
    const qcm = get(currentQcm)
    const qcm2 = get(currentQcm2)
    if (qcm === undefined) return
    let actualQcm = qcm
    if (second === '0' && qcm2 !== undefined) actualQcm = qcm2
    let colorInput = utils.DOM.getById('styleSheetColor')
    if (colorInput === null) {
      colorInput = document.createElement('style')
      colorInput.id = 'styleSheetColor'
      document.body.appendChild(colorInput)
    }
    if (second === 'destroy') {
      colorInput.innerHTML = ''
      return
    }
    const style = `
    ${second === '0' ? '#qcm-container0' : ''}.colored .question:not(.showCorrectAnswers) .answer:nth-of-type(1),
    ${second === '0' ? '#qcm-container0' : ''}.colored .question.showCorrectAnswers .answer:not(.is-correct):nth-of-type(1) {
    background-color: ${actualQcm.colors[0] !== undefined && actualQcm.colors[0] !== '' ? actualQcm.colors[0] : config.defaultColors[0]} !important;
}
${second === '0' ? '#qcm-container0' : ''}.colored .question:not(.showCorrectAnswers) .answer:nth-of-type(2),
${second === '0' ? '#qcm-container0' : ''}.colored .question.showCorrectAnswers .answer:not(.is-correct):nth-of-type(2) {
    background-color: ${actualQcm.colors[1] !== undefined && actualQcm.colors[1] !== '' ? actualQcm.colors[1] : config.defaultColors[1]} !important;
}
${second === '0' ? '#qcm-container0' : ''}.colored .question:not(.showCorrectAnswers) .answer:nth-of-type(3),
${second === '0' ? '#qcm-container0' : ''}.colored .question.showCorrectAnswers .answer:not(.is-correct):nth-of-type(3) {
    background-color: ${actualQcm.colors[2] !== undefined && actualQcm.colors[2] !== '' ? actualQcm.colors[2] : config.defaultColors[2]} !important;
}
${second === '0' ? '#qcm-container0' : ''}.colored .question:not(.showCorrectAnswers) .answer:nth-of-type(4),
${second === '0' ? '#qcm-container0' : ''}.colored .question.showCorrectAnswers .answer:not(.is-correct):nth-of-type(4){
    background-color: ${actualQcm.colors[3] !== undefined && actualQcm.colors[3] !== '' ? actualQcm.colors[3] : config.defaultColors[3]} !important;
}
    `
    if (second === '') colorInput.innerHTML = style
    else if (second === '0' || second === '1') colorInput.innerHTML += style
  },
  GroupList (groups: Group[]): void {
    const dom = utils.DOM.getById('groups-list') as HTMLUListElement
    if (groups.length > 0) {
      dom.innerHTML = ''
      for (const group of groups) {
        const li = utils.DOM.create('li')
        const anchor = utils.DOM.createAnchor({ text: group.name })
        anchor.dataset.id = String(group.id)
        anchor.onclick = () => {
          storage.db.store_data.put({ uid: 'QCMCamlatestSelectedGroup', data: group.id })
          display.GroupStudentsList(group.id, groups)
          this.unActiveGroupList()
          anchor.classList.add('is-active')
        }
        li.appendChild(anchor)
        dom.appendChild(li)
      }
    } else {
      dom.innerHTML = '<li>Pas de groupe</li>'
    }
  },
  unActiveGroupList () {
    document.querySelectorAll('#groups-list.menu-list > li > a').forEach(el => { el.classList.remove('is-active') })
  },
  resetGroupStudentsList () {
    const div = utils.DOM.getById('students-list')
    if (div !== null) {
      div.innerHTML = `<div class="py-6 is-size-3 has-text-grey-light has-text-weight-bold">
      <i class="ri-arrow-left-s-line"></i> Sélectionner un groupe pour voir/éditer ses éléments
    </div>`
    }
  },
  GroupStudentsList (id: number, groups: Group[]): void {
    if (id !== undefined) {
      const dom = utils.DOM.getById('students-list')
      let students: Record<number, Student> = []
      const group = Group.getGroupById(id, groups)
      if (group !== undefined) {
        students = group.students
        if (dom !== null) {
          dom.innerHTML = ''
          const groupNameH4 = document.createElement('h4')
          groupNameH4.className = 'title has-background-grey-lighter p-3'
          groupNameH4.innerText = 'Groupe "'
          const groupNameContent = utils.DOM.create('span', { id: 'group-name-content', text: group.name })
          groupNameContent.onfocus = () => {
            if (groupNameContent.innerText === 'Nom') groupNameContent.innerText = ''
          }
          let groupNameValue = group.name
          groupNameH4.appendChild(groupNameContent)
          groupNameH4.appendChild(document.createTextNode('"'))
          const divStudentsGroupCommands = utils.DOM.create('div', { id: 'students-list-commands' })
          const btnAddStudent = utils.DOM.createButton({ class: 'button', id: 'btn-add-student', html: '<img src="./images/plus-circle-1427-svgrepo-com.svg" style="height:1.5em">', title: 'Ajouter un participant' })
          divStudentsGroupCommands.appendChild(btnAddStudent)
          const btnEditGroupName = utils.DOM.createButton({ class: 'button is-info', id: 'btn-edit-group-name', html: '<i class="ri-edit-line"></i>' })
          divStudentsGroupCommands.appendChild(btnEditGroupName)
          const btnValidateGroupName = utils.DOM.createButton({ class: 'button is-success is-hidden is-light', id: 'btn-validate-edit-group-name', html: '<i class="ri-checkbox-circle-line"></i>' })
          divStudentsGroupCommands.appendChild(btnValidateGroupName)
          const btnCancelGroupName = utils.DOM.createButton({ class: 'button is-danger is-hidden is-light', id: 'btn-cancel-edit-group-name', html: '<i class="ri-close-circle-line"></i>' })
          divStudentsGroupCommands.appendChild(btnCancelGroupName)
          const btnDeleteGroup = utils.DOM.createButton({ class: 'button is-danger', id: 'btn-group-delete', html: '<i class="ri-delete-bin-line"></i>' })
          divStudentsGroupCommands.appendChild(btnDeleteGroup)
          btnAddStudent.onclick = () => {
            this.editNewStudent(dom, group, groups).catch(err => { console.warn('editNewStudent error', err) })
          }
          btnEditGroupName.onclick = () => {
            btnValidateGroupName.classList.remove('is-hidden')
            btnCancelGroupName.classList.remove('is-hidden')
            btnEditGroupName.classList.add('is-hidden')
            groupNameContent.contentEditable = 'true'
            groupNameContent.classList.add('editable')
            groupNameContent.focus()
          }
          btnValidateGroupName.onclick = async () => {
            const groupsNames = []
            for (const agroup of groups) {
              groupsNames.push(agroup.name)
            }
            if (groupNameContent.textContent !== '' && groupNameContent.textContent !== null) {
              if (groupNameContent.textContent === groupNameValue) {
                return
              } else if (groupsNames.includes(groupNameContent.textContent)) {
                alert('Nom de groupe déjà utilisé !')
                return
              } else {
                group.name = groupNameContent.textContent
                // mise à jour de la valeur dans les noms de groupes des élèves.
                for (const aGroup of groups) {
                  for (const key in aGroup.students) {
                    const aStudent = aGroup.students[key]
                    for (const [index, value] of aStudent.groups.entries()) {
                      if (value === groupNameValue) aStudent.groups[index] = group.name
                    }
                  }
                }
                groupNameValue = group.name
                await group.save()
                this.GroupList(groups)
                this.GroupStudentsList(group.id, groups)
              }
            } else {
              alert('Pas de nom défini')
              return
            }
            btnValidateGroupName.classList.add('is-hidden')
            btnCancelGroupName.classList.add('is-hidden')
            btnEditGroupName.classList.remove('is-hidden')
            groupNameContent.contentEditable = 'false'
            groupNameContent.classList.remove('editable')
          }
          btnCancelGroupName.onclick = () => {
            btnValidateGroupName.classList.add('is-hidden')
            btnCancelGroupName.classList.add('is-hidden')
            btnEditGroupName.classList.remove('is-hidden')
            groupNameContent.contentEditable = 'false'
            groupNameContent.classList.remove('editable')
          }
          btnDeleteGroup.onclick = async () => {
            if (confirm('Vous êtes sur le point de supprimer le groupe ' + group.name + '\nConfirmez-vous ?')) {
              for (const [index, group] of groups.entries()) {
                if (group.id === id) {
                  await Group.deleteGroup(group)
                  groups.splice(index, 1)
                  if (storage.isAvailable()) {
                    const data = []
                    for (const agroup of groups) {
                      data.push(agroup.id)
                    }
                    storage.db.store_data.put({ uid: 'QCMCamGroups', data })
                  }
                  this.GroupList(groups)
                  this.resetGroupStudentsList()
                  break
                }
              }
            }
          }
          dom.appendChild(divStudentsGroupCommands)
          dom.appendChild(groupNameH4)
        }
        const alphabeticalStudents = group.alphabeticalStudentIds
        for (const studentId of alphabeticalStudents) {
          const student = students[studentId]
          const divContainerStudent = utils.DOM.create('div', { class: 'columns student p-1 is-vcentered' })
          const header = utils.DOM.create('header', { class: 'column is-3', text: student.name })
          divContainerStudent.appendChild(header)
          const header2 = utils.DOM.create('header', { class: 'column is-3', text: student.firstname })
          divContainerStudent.appendChild(header2)
          const divMarker = utils.DOM.create('div', { class: 'column is-2', text: 'Carte n° ' })
          const spanNumeroMarker = utils.DOM.create('span', { text: String(student.markerId) })
          divMarker.appendChild(spanNumeroMarker)
          divContainerStudent.appendChild(divMarker)
          const divGroup = utils.DOM.create('div', { class: 'column is-2 is-relative', text: 'groupes : ' + student.groups.join(', ') })
          divContainerStudent.appendChild(divGroup)
          const divContainerActions = utils.DOM.create('div', { class: 'column' })
          const divDropDownActions = utils.DOM.create('div', { class: 'dropdown' })
          divContainerActions.appendChild(divDropDownActions)
          const divDropDownTrigger = utils.DOM.create('div', { class: 'dropdown-trigger' })
          divDropDownActions.appendChild(divDropDownTrigger)
          const buttonDropDownTrigger = utils.DOM.createButton({ class: 'button' })
          buttonDropDownTrigger.setAttribute('aria-haspopup', 'true')
          buttonDropDownTrigger.setAttribute('aria-controls', 'dropdown-menu')
          buttonDropDownTrigger.appendChild(utils.DOM.create('span', { text: 'Actions' }))
          buttonDropDownTrigger.appendChild(utils.DOM.create('span', { class: 'icon is-small' }).appendChild(utils.DOM.create('i', { class: 'ri-arrow-down-s-line' })))
          buttonDropDownTrigger.onclick = () => {
            const hisClass = divDropDownActions.classList
            const isActive = hisClass.contains('is-active')
            document.querySelectorAll('.student .dropdown.is-active').forEach(el => { el.classList.remove('is-active') })
            if (!isActive) divDropDownActions.classList.add('is-active')
          }
          divDropDownTrigger.appendChild(buttonDropDownTrigger)
          const divDropDownMenu = utils.DOM.create('div', { class: 'dropdown-menu', id: 'userEdit-' + String(student.markerId) })
          divDropDownMenu.setAttribute('role', 'menu')
          divDropDownActions.appendChild(divDropDownMenu)
          const divDropDownContent = utils.DOM.create('div', { class: 'dropdown-content' })
          divDropDownMenu.appendChild(divDropDownContent)
          const buttonPrintMarker = utils.DOM.createAnchor({ class: 'dropdown-item' })
          const iPrint = utils.DOM.create('i', { class: 'ri-printer-line' })
          buttonPrintMarker.appendChild(iPrint)
          buttonPrintMarker.appendChild(document.createTextNode(' Marqueur'))
          buttonPrintMarker.onclick = () => {
            divDropDownActions.classList.remove('is-active')
            const fenetre = window.open('generator.html?id=' + String(student.markerId) + '&name=' + student.name + (student.firstname !== '' ? ' ' + student.firstname : ''))
            if (fenetre !== null) fenetre.onclose = () => { return false }
          }
          divDropDownContent.appendChild(buttonPrintMarker)
          const buttonEdit = utils.DOM.createAnchor({ class: 'dropdown-item' })
          const i = utils.DOM.create('i', { class: 'ri-edit-line' })
          buttonEdit.appendChild(i)
          buttonEdit.appendChild(document.createTextNode(' Éditer'))
          buttonEdit.onclick = () => {
            if (header.contentEditable === 'true') return false
            let changes = false
            const name = student.name
            const firstname = student.firstname
            const markerId = student.markerId
            const groupList = student.groups
            divDropDownActions.classList.remove('is-active')
            divDropDownActions.classList.add('is-hidden')
            header.contentEditable = 'true'
            header2.contentEditable = 'true'
            spanNumeroMarker.innerHTML = ''
            const listOfDisponibleMarkers = group.getListOfDisponibleMarkers()
            listOfDisponibleMarkers.unshift(0, student.markerId)
            const selectMarkerId = this.createSelect(listOfDisponibleMarkers, student.markerId)
            selectMarkerId.classList.add('select')
            selectMarkerId.onchange = () => { student.markerId = Number(selectMarkerId.value) }
            spanNumeroMarker.appendChild(selectMarkerId)
            header.classList.add('has-background-link-light')
            header2.classList.add('has-background-link-light')
            const editGroupsButton = utils.DOM.createButton({ html: '<i class="ri-user-follow-line"></i>', class: 'button is-small' })
            divGroup.appendChild(editGroupsButton)
            const modal = this.createGroupCheckList(student, groups)
            modal.classList.add('is-hidden', 'modal-select-student-groups')
            divGroup.appendChild(modal)
            editGroupsButton.onclick = () => {
              modal.classList.toggle('is-hidden')
            }
            const confirmButton = utils.DOM.createButton({ html: '<i class="ri-checkbox-circle-line"></i>', class: 'is-success button ml-3 is-outlined is-small', title: 'Valider les modifications' })
            divContainerActions.appendChild(confirmButton)
            const cancelButton = utils.DOM.createButton({ html: '<i class="ri-close-circle-line"></i>', class: 'is-danger button ml-3 is-outlined is-small', title: 'Annuler les modifications' })
            divContainerActions.appendChild(cancelButton)
            confirmButton.onclick = async () => {
              header.contentEditable = 'false'
              header.classList.remove('has-background-link-light')
              header2.contentEditable = 'false'
              header2.classList.remove('has-background-link-light')
              spanNumeroMarker.innerText = String(student.markerId)
              if (header.innerText !== '') student.name = header.innerText
              if (header2.innerText !== '') student.firstname = header2.innerText
              if (name !== student.name) changes = true
              if (firstname !== student.firstname) changes = true
              if (markerId !== student.markerId) changes = true
              const StudentGroupList = student.groups.slice().sort()
              if (!(groupList.length === StudentGroupList.length && groupList.slice().sort().every((value, index) => value === StudentGroupList[index]))) changes = true
              divGroup.removeChild(editGroupsButton)
              divGroup.removeChild(modal)
              divGroup.innerText = 'groupes : ' + student.groups.join((', '))
              divContainerActions.removeChild(confirmButton)
              divContainerActions.removeChild(cancelButton)
              divDropDownActions.classList.remove('is-hidden')
              // sauvegarde
              if (changes && storage.isAvailable()) {
                await student.save()
              }
              this.GroupStudentsList(group.id, groups)
            }
            cancelButton.onclick = () => {
              header.contentEditable = 'false'
              header.classList.remove('has-background-link-light')
              header2.contentEditable = 'false'
              header2.classList.remove('has-background-link-light')
              spanNumeroMarker.innerText = String(markerId)
              header.innerText = name
              header2.innerText = firstname
              divGroup.removeChild(editGroupsButton)
              divGroup.removeChild(modal)
              divGroup.innerText = 'groupes :' + groupList.join(', ')
              student.groups = groupList
              divContainerActions.removeChild(cancelButton)
              divContainerActions.removeChild(confirmButton)
              divDropDownActions.classList.remove('is-hidden')
            }
          }
          divDropDownContent.appendChild(buttonEdit)
          const buttonRemove = utils.DOM.createAnchor({ class: 'dropdown-item' })
          const i2 = utils.DOM.create('i', { class: 'ri-user-unfollow-line' })
          buttonRemove.appendChild(i2)
          buttonRemove.appendChild(document.createTextNode(' Supprimer'))
          buttonRemove.onclick = async () => {
            divDropDownActions.classList.remove('is-active')
            if (confirm('Vous allez supprimer ' + student.name + '.\nConfirmez-vous ?')) {
              await group.removeStudentById(student.id)
              this.GroupStudentsList(id, groups)
            }
          }
          divDropDownContent.appendChild(buttonRemove)
          divContainerStudent.appendChild(divContainerActions)
          if (dom !== null) {
            dom.appendChild(divContainerStudent)
          }
        }
      }
    }
  },
  async GroupImportString (result: string, groups: Group[]): Promise<void> {
    if (result === '') return
    let importGroup: Group | undefined
    let groupNumber = 0
    let userNumber = 0
    const groupNames: string[] = []
    if (groups.length > 0) {
      if (!confirm('Ok pour ajouter les groupes\nAnnuler pour remplacer les groupes.')) {
        if (storage.isAvailable()) {
          // delete all groups and students
          for (const group of groups) {
            await Group.deleteGroup(group)
          }
          groups = []
        }
      } else {
        for (const group of groups) {
          groupNames.push(group.name)
        }
      }
    }
    const defaultGroupName = 'import'
    const markersId: Record<string, number> = { import: 0 }
    // afficher les données d'une ligne pour effectuer les rapprochements de champs
    const selectIndexes: Record<number, string> = { 1: 'prenom', 2: 'nom', 3: 'groupe', 4: 'marqueur' }
    const correspondances: Record<number, number> = {}
    // prendre la première ligne
    const lines = result.split(/[\r\n]+/g)
    const line0 = lines[0].split(/[\t;,]/g)
    const line1 = lines[1].split(/[\t;,]/g)
    const selectContent = `
    <option value=''>Choix</option>
    <option>Prénom</option>
    <option>NOM</option>
    <option>groupe</option>
    <option>marqueur</option>
    `
    const domImport = document.getElementById('import-2nd')
    domImport?.classList.remove('is-hidden')
    if (domImport !== null) domImport.innerHTML = ''
    const table = document.createElement('table')
    const thead = document.createElement('thead')
    table.appendChild(thead)
    const TR1 = document.createElement('tr')
    thead.appendChild(TR1)
    const TD1 = document.createElement('td')
    TR1.appendChild(TD1)
    const TD2 = document.createElement('td')
    TR1.appendChild(TD2)
    const TD3 = document.createElement('td')
    TR1.appendChild(TD3)
    const TR2 = document.createElement('tr')
    thead.appendChild(TR2)
    const TH1 = document.createElement('th')
    TH1.innerText = 'Ligne 1'
    const TH2 = document.createElement('th')
    TH2.innerText = 'Ligne 2'
    const TH3 = document.createElement('th')
    TH3.innerText = 'Corresp.'
    TR2.appendChild(TH1)
    TR2.appendChild(TH2)
    TR2.appendChild(TH3)
    const chckbxIgnoreL0 = document.createElement('input')
    chckbxIgnoreL0.type = 'checkbox'
    chckbxIgnoreL0.id = 'ignorel0'
    chckbxIgnoreL0.value = 'true'
    chckbxIgnoreL0.title = 'ignorer'
    TD1.appendChild(chckbxIgnoreL0)
    const chckbxIgnoreL1 = document.createElement('input')
    chckbxIgnoreL1.type = 'checkbox'
    chckbxIgnoreL1.id = 'ignorel1'
    chckbxIgnoreL1.value = 'true'
    chckbxIgnoreL1.title = 'ignorer'
    TD2.appendChild(chckbxIgnoreL1)
    TD3.innerText = '<- Ignorer'
    const tbody = document.createElement('tbody')
    table.appendChild(tbody)
    for (const [index, txt] of line0.entries()) {
      const tr = document.createElement('tr')
      const td0 = document.createElement('td')
      td0.innerText = txt
      const td1 = document.createElement('td')
      td1.innerText = line1[index]
      const td2 = document.createElement('td')
      const select = document.createElement('select')
      select.id = 'selectCorresp' + String(index)
      select.innerHTML = selectContent
      select.onchange = () => {
        if (select.selectedIndex > 0) {
          select.classList.add('green')
          correspondances[index] = select.selectedIndex
        } else {
          select.classList.remove('green')
          correspondances[index] = -1
        }
      }
      td2.appendChild(select)
      tr.appendChild(td0)
      tr.appendChild(td1)
      tr.appendChild(td2)
      tbody.appendChild(tr)
    }
    domImport?.appendChild(table)
    const newBtn = document.createElement('button')
    newBtn.type = 'submit'
    newBtn.classList.add('button', 'is-success')
    newBtn.id = 'btn-import-students'
    newBtn.innerText = 'Valider'
    domImport?.appendChild(newBtn)
    newBtn.onclick = async (evt) => {
      evt.preventDefault()
      for (const [index, line] of lines.entries()) {
        if (chckbxIgnoreL0.checked && index === 0) continue
        if (chckbxIgnoreL1.checked && index === 1) continue
        if (line === '') continue
        const values = line.split(/[\t;,]/g)
        const newUser: Record<string, string> = {}
        for (const [ind, value] of values.entries()) {
          if (correspondances[ind] > 0 && correspondances[ind] < 5) {
            newUser[selectIndexes[correspondances[ind]]] = value.trim()
          }
        }
        if (newUser.marqueur === undefined) {
          let markerId = 0
          if (newUser.groupe !== undefined) {
            if (markersId[newUser.groupe] === undefined) {
              markersId[newUser.groupe] = 0
            }
            markerId = ++markersId[newUser.groupe]
          } else {
            markerId = ++markersId[defaultGroupName]
          }
          newUser.marqueur = String(markerId)
        }
        if (newUser.groupe === undefined) {
          if (importGroup === undefined) {
            importGroup = new Group('Groupe importé')
            await importGroup.init()
          }
          await importGroup.addStudent(newUser.nom, newUser.prenom, Number(newUser.marqueur), [defaultGroupName])
        } else {
          if (!groupNames.includes(newUser.groupe)) {
            groupNumber++
            groupNames.push(newUser.groupe)
            const group = new Group(newUser.groupe)
            await group.init()
            console.log(group.id)
            groups.push(group)
          }
          await groups[groupNames.indexOf(newUser.groupe)].addStudent(newUser.nom, newUser.prenom, Number(newUser.marqueur), [newUser.groupe])
          userNumber++
        }
      }
      if (importGroup !== undefined) groups.push(importGroup)
      if (domImport !== null) {
        domImport.innerHTML = `<div class="has-text-danger has-text-weight-bold is-family-sans-serif">Import d${groupNumber > 1 ? 'e ' : '’'}${groupNumber} groupe${groupNumber > 1 ? 's' : ''} et de ${userNumber} élèves effectué</div>`
      }
      // sauvegarde de tous les groupes
      for (const group of groups) {
        await group.save()
      }
      this.GroupList(groups)
    }
  },
  async editNewStudent (dom: HTMLElement, group: Group, groups: Group[]): Promise<void> {
    const student = new Student('Nom', 'Prénom', [group.name], 0)
    await student.init(group.id)
    const divContainerStudent = utils.DOM.create('div', { class: 'columns student p-1 is-vcentered' })
    const header = utils.DOM.create('header', { class: 'column is-3', text: student.name })
    divContainerStudent.appendChild(header)
    const header2 = utils.DOM.create('header', { class: 'column is-2', text: student.firstname })
    divContainerStudent.appendChild(header2)
    const divMarker = utils.DOM.create('div', { class: 'column is-2', text: 'Marqueur n° ' })
    const spanNumeroMarker = utils.DOM.create('span', { text: String(student.markerId) })
    divMarker.appendChild(spanNumeroMarker)
    divContainerStudent.appendChild(divMarker)
    const divGroup = utils.DOM.create('div', { class: 'column is-2 is-relative', text: 'groupes : ' + student.groups.join(', ') })
    divContainerStudent.appendChild(divGroup)
    const divContainerActions = utils.DOM.create('div', { class: 'column' })
    const buttonPrintMarker = utils.DOM.createAnchor({ class: 'button' })
    const iPrint = utils.DOM.create('i', { class: 'ri-printer-line' })
    buttonPrintMarker.appendChild(iPrint)
    buttonPrintMarker.appendChild(document.createTextNode(' Marqueur'))
    buttonPrintMarker.onclick = () => {
      const fenetre = window.open('generator.html?id=' + String(student.markerId) + '&name=' + student.name)
      if (fenetre !== null) fenetre.onclose = () => { return false }
    }
    divContainerActions.appendChild(buttonPrintMarker)
    header.contentEditable = 'true'
    header.onfocus = () => {
      if (header.innerText === 'Nom') header.innerText = ''
    }
    header2.contentEditable = 'true'
    header2.onfocus = () => {
      if (header2.innerText === 'Prénom') header2.innerText = ''
    }
    spanNumeroMarker.innerHTML = ''
    const listOfDisponibleMarkers = group.getListOfDisponibleMarkers()
    const selectMarkerId = this.createSelect(listOfDisponibleMarkers, student.markerId)
    selectMarkerId.classList.add('select')
    selectMarkerId.onchange = () => { student.markerId = Number(selectMarkerId.value) }
    spanNumeroMarker.appendChild(selectMarkerId)
    header.classList.add('has-background-link-light')
    header.classList.add('mx-2')
    header2.classList.add('has-background-link-light')
    header2.classList.add('mx-2')
    const editGroupsButton = utils.DOM.createButton({ html: '<i class="ri-user-follow-line"></i>', class: 'button is-small' })
    divGroup.appendChild(editGroupsButton)
    const modal = this.createGroupCheckList(student, groups)
    modal.classList.add('is-hidden', 'modal-select-student-groups')
    divGroup.appendChild(modal)
    editGroupsButton.onclick = () => {
      modal.classList.toggle('is-hidden')
    }
    const confirmButton = utils.DOM.createButton({ html: '<i class="ri-checkbox-circle-line"></i>', class: 'is-success button ml-3 is-outlined is-small', title: 'Valider les modifications' })
    divContainerActions.appendChild(confirmButton)
    const cancelButton = utils.DOM.createButton({ html: '<i class="ri-close-circle-line"></i>', class: 'is-danger button ml-3 is-outlined is-small', title: 'Annuler les modifications' })
    divContainerActions.appendChild(cancelButton)
    confirmButton.onclick = async () => {
      if (header.innerText === 'Nom' || header.innerText === '') {
        if (confirm('Vous n\'avez pas donné de nom à l\'utilisateur !\nConfirmer ?')) {
          return false
        }
      }
      student.markerId = Number(selectMarkerId.value)
      student.name = header.innerText
      student.firstname = header2.innerText
      await student.save()
      group.pushStudent(student)
      this.GroupStudentsList(group.id, groups)
    }
    cancelButton.onclick = async () => {
      await group.removeStudentById(student.id)
      dom.removeChild(divContainerStudent)
    }
    divContainerStudent.appendChild(divContainerActions)
    const h4GroupName = document.querySelector('#students-list > h4')
    if (dom !== null && h4GroupName !== null && h4GroupName.parentNode !== null) {
      h4GroupName.parentNode.insertBefore(divContainerStudent, h4GroupName.nextSibling)
    }
  },
  createStudentCard (student: Student): HTMLElement {
    const card = utils.DOM.create('article', { class: 'student pointer', id: 'gp-student' + String(student.markerId) })
    card.dataset.markerId = String(student.markerId)
    const cardMarker = utils.DOM.create('div', { class: 'markerid', text: String(student.markerId) })
    const cardName = utils.DOM.create('div', { class: 'studentname', text: (student.name ?? '') + ' ' + (student.firstname ?? '') })
    card.appendChild(cardMarker)
    card.appendChild(cardName)
    return card
  },
  displaySubgroups (group: Group, $containerA: HTMLElement, $containerB: HTMLElement): void {
    const containers = [$containerA, $containerB]
    for (const [key, subgroup] of group.subgroups.entries()) {
      containers[key].innerHTML = ''
      for (const studentId of subgroup) {
        const student = group.students[studentId]
        const $student = this.createStudentCard(student)
        $student.onclick = () => {
          group.moveToSubgroup(1 - key, student.id)
          this.displaySubgroups(group, $containerA, $containerB)
        }
        containers[key].appendChild($student)
      }
    }
  },
  createSubgroupsByVote (): void {
    const group = get(currentGroup)
    if (group === undefined) { console.warn('createSubgroupsByVote', 'currentGroup is undefined'); return }
    this.startVoteForSubgroup(group)
  },
  startVoteForSubgroup (group: Group): void {
    if (group === undefined) { console.warn('startVoteForSubgroup', 'group is undefined'); return }
    if (tictoc !== null) {
      clearInterval(tictoc)
      tictoc = null
    }
    tictoc = setInterval(() => {
      const mks: Record<number, string> | undefined = detection.detect()
      if (utils.object.getLength(mks) > 0 && mks !== undefined) {
        group.setSubgroupsByVotes(mks)
        this.displaySubgroups(group, utils.DOM.getById('gpA-container') as HTMLElement, utils.DOM.getById('gpB-container') as HTMLElement)
      }
    }, 100)
  },
  async confirmSubGroups (): Promise<void> {
    const group = get(currentGroup)
    if (group === undefined) { console.warn('confirmSubGroups', 'currentGroup is undefined'); return }
    await group.save()
  },
  QCMList (qcms: QcmList): void {
    if (qcms === undefined) return
    const dom = utils.DOM.getById('section-qcms-0')
    if (dom !== null) {
      dom.innerHTML = ''
      qcms.getMiniatures().then(miniatures => {
        for (const [index, question] of miniatures.entries()) {
          dom.appendChild(this.createQcmCard(qcms, index, question))
        }
        this.renderLatex('section-qcms-0')
        this.setQCMCardsFontSize()
      }).catch(err => { console.error('Erreur récupération miniatures', err) })
    }
  },
  unActiveQCMList (): void {
    document.querySelectorAll('#qcm-list.menu-list > li > a').forEach(el => { el.classList.remove('is-active') })
  },
  async QCMQuestionsList (name: string, qcm: Qcm | undefined, qcms: QcmList, index: number): Promise<void> {
    showModal('waitAction')
    this.inlineEditors = []
    utils.DOM.addClass('qm-qcm', 'is-hidden')
    utils.DOM.removeClass('qm-qcm-editor', 'is-hidden')
    const editorQCMName = utils.DOM.create('div', { id: 'editor-qcm-name', text: name })
    const questionListName = utils.DOM.getById('question-list-name')
    if (questionListName !== null) {
      questionListName.innerHTML = ''
      questionListName.appendChild(editorQCMName)
      if (qcm !== undefined) {
        const { default: InlineEditor } = await import('../js/ckeditor5/build/ckeditor')
        InlineEditor.create(editorQCMName, {
          toolbar: ['undo', 'redo']
        }).then(
          (editor: any) => {
            this.inlineEditors.push(editor)
            editor.editing.view.document.on('blur', () => {
              const text = utils.text.extractTextFromTags(editor.getData())
              if (qcm.name !== text) {
                qcm.name = text
                qcm.save()
                qcms.list[index][1] = text
                qcms.save()
                qcms.updateQcm(qcm)
              }
            })
          }
        ).catch((error: any) => { console.error('Pb création éditeur', error) })
      }
    }
    const dom = utils.DOM.getById('questions-list')
    if (dom !== null) {
      dom.innerHTML = ''
      if (qcm !== undefined) {
        currentQcm.set(qcm)
        display.updateQCMColors()
        editorQCMName.dataset.id = qcm.id
        const container = utils.DOM.create('div', { class: 'container' })
        const switchColors = utils.DOM.getById('switchIsQCMColored') as HTMLInputElement
        const colorEditorQCM = utils.DOM.getById('ColorEditorQCM') as HTMLElement
        if (qcm.coloredAnswers) {
          switchColors.checked = true
          colorEditorQCM.classList.remove('is-hidden')
          colorEditorQCM.classList.add('is-inline-block')
        } else {
          switchColors.checked = false
          colorEditorQCM.classList.add('is-hidden')
          colorEditorQCM.classList.remove('is-inline-block')
        }
        qcm.loadQuestions().then(async questions => {
          currentQuestions = questions
          for (const [index, question] of questions.entries()) {
            container.appendChild(await this.displayQuestionEditor(question, index, qcm, qcms, name))
          }
          dom.appendChild(container)
          const divImageInCenter = utils.DOM.create('div', { class: 'has-text-centered pointer btn-add-answer' })
          const imagePlus = utils.DOM.createImage({ src: './images/plus-circle-1427-svgrepo-com.svg', title: 'Ajouter une question' })
          divImageInCenter.appendChild(imagePlus)
          divImageInCenter.onclick = async () => {
            // création des deux premières questions
            const newQuestion = qcm.createQuestion('<h3>Nouvelle question</h3>')
            newQuestion.addAnwser('')
            newQuestion.addAnwser('')
            newQuestion.save()
            qcm.save()
            qcms.updateQcm(qcm)
            container.appendChild(await this.displayQuestionEditor(newQuestion, qcm.questionsList.length - 1, qcm, qcms, ''))
          }
          dom.appendChild(divImageInCenter)
          // TODO : à revoir !!! ckeditor enregistre la taille des images et les restitue après le changement :(
          setTimeout(() => {
            /** utils.image.calculateSizeInEm('questions-list')
            utils.image.makeEditable('qm-qcm-editor') */
            this.hideModal(document.getElementById('qm-waitAction'))
          }, 1200)
          this.setQuestionFontSize()
        }).catch(err => { console.error('Erreur de chargement des questions', err) })
        // this.renderLatex('questions-list')
      } else dom.appendChild(document.createTextNode('?! Questionnaire non trouvé !?'))
    } else {
      this.hideModal(document.getElementById('qm-waitAction'))
    }
  },
  async refreshQuestions (qcm: Qcm, qcms: QcmList, name: string): Promise<void> {
    const dom = utils.DOM.getById('questions-list')
    if (dom !== null) {
      dom.innerHTML = ''
      const container = utils.DOM.create('div', { class: 'container' })
      for (const [index, question] of currentQuestions.entries()) {
        container.appendChild(await this.displayQuestionEditor(question, index, qcm, qcms, name))
      }
      dom.appendChild(container)
      const divImageInCenter = utils.DOM.create('div', { class: 'has-text-centered pointer btn-add-answer' })
      const imagePlus = utils.DOM.createImage({ src: './images/plus-circle-1427-svgrepo-com.svg', title: 'Ajouter une question' })
      divImageInCenter.appendChild(imagePlus)
      divImageInCenter.onclick = async () => {
        // création des deux premières questions
        const newQuestion = qcm.createQuestion('<h3>Nouvelle question</h3>')
        newQuestion.addAnwser('')
        newQuestion.addAnwser('')
        newQuestion.save()
        qcm.save()
        container.appendChild(await this.displayQuestionEditor(newQuestion, qcm.questionsList.length - 1, qcm, qcms, ''))
      }
      dom.appendChild(divImageInCenter)
      /** setTimeout(() => {
        utils.image.calculateSizeInEm('questions-list')
        utils.image.makeEditable('qm-qcm-editor')
      }, 300) */
      this.setQuestionFontSize()
    }
  },
  async displayQuestionEditor (question: Question, index: number, qcm: Qcm, qcms: QcmList, name: string) {
    const container = utils.DOM.create('article')
    const qcmId = qcms.getQcmIndex(qcm.id)
    let timeOutTextSizer: undefined | number
    let timeOutSaveTimer: undefined | number
    const questionNumero = utils.DOM.create('h2', { text: 'Question n°' + String(index + 1), class: 'title has-background-grey-light p-2' })
    container.appendChild(questionNumero)
    const buttonDelete = utils.DOM.createButton({ class: 'button ml-4 is-pulled-right', html: '<i class="ri-delete-bin-line"></i>' })
    display.switchIsQCMColored(qcm.coloredAnswers)
    buttonDelete.onclick = () => {
      if (confirm('Supprimer la question ' + String(index + 1) + ' ?')) {
        qcm.removeQuestion(index)
        qcm.save()
        qcms.updateQcm(qcm)
        currentQuestions?.splice(index, 1)
        this.refreshQuestions(qcm, qcms, name)
      }
    }
    questionNumero.appendChild(buttonDelete)
    const buttonCancel = utils.DOM.createButton({ class: 'button ml-4 is-pulled-right has-text-danger is-hidden paste-btn', html: '<i class="ri-file-close-line"></i>' })
    buttonCancel.title = 'Annuler la copie de question'
    buttonCancel.onclick = () => {
      removeCopyCutClasses()
      questionToCopy = ''
      questionToCut = { question: '', QCMfrom: '', index: 0 }
    }
    questionNumero.appendChild(buttonCancel)
    const buttonCopy = utils.DOM.createButton({ class: 'button ml-4 is-pulled-right copy-btn', html: '<i class="ri-file-copy-line"></i>' })
    buttonCopy.title = 'Copier la question'
    buttonCopy.onclick = () => {
      utils.DOM.addClass('questions-list', 'copy-mode')
      questionContainer.classList.add('copied')
      questionToCopy = question.id
    }
    questionNumero.appendChild(buttonCopy)
    const buttonCut = utils.DOM.createButton({ class: 'button ml-4 is-pulled-right copy-btn', html: '<i class="ri-scissors-line"></i>' })
    buttonCut.title = 'Couper la question'
    buttonCut.onclick = () => {
      utils.DOM.addClass('questions-list', 'copy-mode')
      questionContainer.classList.add('cutted')
      questionToCut = { question: question.id, QCMfrom: qcm.id, index }
    }
    questionNumero.appendChild(buttonCut)
    const buttonPasteBefore = utils.DOM.createButton({ class: 'button ml-4 is-pulled-right paste-btn is-hidden', html: '<i class="ri-file-upload-line"></i>' })
    buttonPasteBefore.title = 'Coller la question avant'
    buttonPasteBefore.onclick = async () => {
      if (questionToCopy !== '') {
        const newQuestionId = await Question.duplicate(questionToCopy)
        if (newQuestionId !== '') {
          qcm.questionsList.splice(index, 0, newQuestionId)
          const newQuestion = await Question.load(newQuestionId)
          if (newQuestion !== undefined) {
            currentQuestions?.splice(index, 0, newQuestion)
          }
          qcm.save()
          qcms.updateQcm(qcm)
          this.refreshQuestions(qcm, qcms, name)
        }
      } else if (questionToCut.question !== '') {
        // insertion
        qcm.questionsList.splice(index, 0, questionToCut.question)
        qcm.save()
        const thisQuestion = await Question.load(questionToCut.question)
        if (thisQuestion !== undefined) {
          currentQuestions?.splice(index, 0, thisQuestion)
        }
        qcms.updateQcm(qcm)
        // suppression de l'ancien QCM
        const QCMfrom = await Qcm.load(questionToCut.QCMfrom)
        QCMfrom.questionsList.splice(questionToCut.index, 1)
        QCMfrom.save()
        qcms.updateQcm(QCMfrom)
        this.refreshQuestions(qcm, qcms, name)
        questionToCut = { question: '', QCMfrom: '', index: 0 }
        removeCopyCutClasses()
      }
    }
    questionNumero.appendChild(buttonPasteBefore)
    const buttonPasteAfter = utils.DOM.createButton({ class: 'button ml-4 is-pulled-right paste-btn is-hidden', html: '<i class="ri-file-download-line"></i>' })
    buttonPasteAfter.title = 'Coller la question après'
    buttonPasteAfter.onclick = async () => {
      if (questionToCopy !== '') {
        const newQuestionId = await Question.duplicate(questionToCopy)
        if (newQuestionId !== '') {
          qcm.questionsList.splice(index + 1, 0, newQuestionId)
          qcm.save()
          qcms.updateQcm(qcm)
          const newQuestion = await Question.load(newQuestionId)
          if (newQuestion !== undefined) {
            currentQuestions?.splice(index + 1, 0, newQuestion)
          }
          this.refreshQuestions(qcm, qcms, name)
        }
      } else if (questionToCut.question !== '') {
        // insertion
        qcm.questionsList.splice(index + 1, 0, questionToCut.question)
        qcm.save()
        const thisQuestion = await Question.load(questionToCut.question)
        if (thisQuestion !== undefined) {
          currentQuestions?.splice(index + 1, 0, thisQuestion)
        }
        qcms.updateQcm(qcm)
        // suppression de l'ancien QCM
        const QCMfrom = await Qcm.load(questionToCut.QCMfrom)
        QCMfrom.questionsList.splice(questionToCut.index, 1)
        QCMfrom.save()
        qcms.updateQcm(QCMfrom)
        this.refreshQuestions(qcm, qcms, name)
        questionToCut = { question: '', QCMfrom: '', index: 0 }
        removeCopyCutClasses()
      }
    }
    function removeCopyCutClasses (): void {
      document.querySelector('#questions-list .copied')?.classList.remove('copied')
      document.querySelector('#questions-list .cutted')?.classList.remove('cutted')
      utils.DOM.removeClass('questions-list', 'copy-mode')
    }
    questionNumero.appendChild(buttonPasteAfter)
    const questionCommands = utils.DOM.create('div', { class: 'question-commands' })
    const questionContainer = utils.DOM.create('div', { class: 'question' + ' ' + question.displayMode, id: 'questionInList' + String(index) })
    if (!isNaN(question.textSize)) questionContainer.style.fontSize = String(question.textSize) + 'em'
    const div = utils.DOM.create('div')
    /* display mode */
    const divDropDownActions = utils.DOM.create('div', { class: 'dropdown ml-3' })
    div.appendChild(divDropDownActions)
    const divDropDownTrigger = utils.DOM.create('div', { class: 'dropdown-trigger' })
    divDropDownActions.appendChild(divDropDownTrigger)
    const buttonDropDownTrigger = utils.DOM.createButton({ class: 'button' })
    buttonDropDownTrigger.setAttribute('aria-haspopup', 'true')
    buttonDropDownTrigger.setAttribute('aria-controls', 'dropdown-menu')
    buttonDropDownTrigger.appendChild(utils.DOM.create('span', { text: 'Disposition' }))
    buttonDropDownTrigger.appendChild(utils.DOM.create('span', { class: 'icon is-small' }).appendChild(utils.DOM.create('i', { class: 'ri-arrow-down-s-line' })))
    buttonDropDownTrigger.onclick = () => {
      const hisClass = divDropDownActions.classList
      const isActive = hisClass.contains('is-active')
      document.querySelectorAll('#questions-list-container .dropdown.is-active').forEach(el => { el.classList.remove('is-active') })
      if (!isActive) divDropDownActions.classList.add('is-active')
    }
    divDropDownTrigger.appendChild(buttonDropDownTrigger)
    const divDropDownMenu = utils.DOM.create('div', { class: 'dropdown-menu', id: 'questionEdit-' + String(index) })
    divDropDownMenu.setAttribute('role', 'menu')
    divDropDownActions.appendChild(divDropDownMenu)
    const divDropDownContent = utils.DOM.create('div', { class: 'dropdown-content' })
    divDropDownMenu.appendChild(divDropDownContent)
    const buttonDisplayClassic = utils.DOM.createAnchor({ class: 'dropdown-item' })
    const iPrint = utils.DOM.create('i', { class: 'ri-list-check' })
    buttonDisplayClassic.appendChild(iPrint)
    buttonDisplayClassic.appendChild(document.createTextNode(' liste de texte'))
    if (question.displayMode === 'classic') buttonDisplayClassic.classList.add('is-active')
    buttonDisplayClassic.onclick = () => {
      divDropDownActions.classList.toggle('is-active')
      unactivDropDownActionsButtons()
      buttonDisplayClassic.classList.add('is-active')
      question.displayMode = 'classic'
      questionContainer.classList.remove('imageAbove', 'imageAside', 'imagesInAnswers', 'answerWithoutContent')
      question.save()
    }
    divDropDownContent.appendChild(buttonDisplayClassic)
    const buttonImage = utils.DOM.createAnchor({ class: 'dropdown-item' })
    buttonImage.appendChild(utils.DOM.create('i', { class: 'ri-image-line' }))
    buttonImage.appendChild(document.createTextNode(' Image au dessus'))
    if (question.displayMode === 'imageAbove') buttonImage.classList.add('is-active')
    buttonImage.onclick = () => {
      divDropDownActions.classList.toggle('is-active')
      unactivDropDownActionsButtons()
      buttonImage.classList.add('is-active')
      question.displayMode = 'imageAbove'
      questionContainer.classList.remove('imageAside', 'imagesInAnswers', 'answerWithoutContent', 'classic')
      questionContainer.classList.add('imageAbove')
      question.save()
    }
    divDropDownContent.appendChild(buttonImage)
    const buttonImageAside = utils.DOM.createAnchor({ class: 'dropdown-item' })
    buttonImageAside.appendChild(utils.DOM.create('i', { class: 'ri-indent-decrease' }))
    buttonImageAside.appendChild(document.createTextNode(' Image à gauche'))
    if (question.displayMode === 'imageAside') buttonImageAside.classList.add('is-active')
    buttonImageAside.onclick = () => {
      divDropDownActions.classList.toggle('is-active')
      unactivDropDownActionsButtons()
      buttonImageAside.classList.add('is-active')
      question.displayMode = 'imageAside'
      questionContainer.classList.remove('imageAbove', 'imagesInAnswers', 'classic', 'answerWithoutContent')
      questionContainer.classList.add('imageAside')
      question.save()
    }
    divDropDownContent.appendChild(buttonImageAside)
    const buttonImagesInAnswers = utils.DOM.createAnchor({ class: 'dropdown-item' })
    const i2 = utils.DOM.create('i', { class: 'ri-gallery-line' })
    buttonImagesInAnswers.appendChild(i2)
    buttonImagesInAnswers.appendChild(document.createTextNode(' Images dans les réponses'))
    if (question.displayMode === 'imagesInAnswers') buttonImagesInAnswers.classList.add('is-active')
    buttonImagesInAnswers.onclick = () => {
      divDropDownActions.classList.toggle('is-active')
      unactivDropDownActionsButtons()
      buttonImagesInAnswers.classList.add('is-active')
      question.displayMode = 'imagesInAnswers'
      questionContainer.classList.remove('imageAbove', 'imageAside', 'answerWithoutContent', 'classic')
      questionContainer.classList.add('imagesInAnswers')
      question.save()
    }
    divDropDownContent.appendChild(buttonImagesInAnswers)
    const buttonAnswerWithoutContent = utils.DOM.createAnchor({ class: 'dropdown-item' })
    buttonAnswerWithoutContent.appendChild(utils.DOM.create('i', { class: 'ri-rectangle-line' }))
    buttonAnswerWithoutContent.appendChild(document.createTextNode(' Réponses vides'))
    if (question.displayMode === 'answerWithoutContent') buttonAnswerWithoutContent.classList.add('is-active')
    buttonAnswerWithoutContent.onclick = () => {
      divDropDownActions.classList.toggle('is-active')
      unactivDropDownActionsButtons()
      buttonAnswerWithoutContent.classList.add('is-active')
      question.displayMode = 'answerWithoutContent'
      questionContainer.classList.remove('imageAbove', 'imageAside', 'imagesInAnswers', 'classic')
      questionContainer.classList.add('answerWithoutContent')
      question.save()
    }
    function unactivDropDownActionsButtons (): void {
      [buttonAnswerWithoutContent, buttonDisplayClassic, buttonImage, buttonImageAside, buttonImagesInAnswers].forEach(el => { el.classList.remove('is-active') })
    }
    divDropDownContent.appendChild(buttonAnswerWithoutContent)
    div.appendChild(divDropDownActions)
    /* shuffle mode */
    const shuffleModeQuestion = utils.DOM.createSwitchButton('shuffleModeQuestion' + String(index))
    if (question.shuffleMode) shuffleModeQuestion.checked = true
    shuffleModeQuestion.title = 'Mélange l’ordre des réponses'
    shuffleModeQuestion.onclick = () => {
      question.shuffleMode = shuffleModeQuestion.checked
      question.save()
    }
    div.appendChild(shuffleModeQuestion)
    div.appendChild(utils.DOM.createSwitchLabelButton('shuffleModeQuestion' + String(index), 'Mélanger'))
    /* answer mode */
    const multipleAnswerModeQuestion = utils.DOM.createSwitchButton('multipleAnswerModeQuestion' + String(index), 'is-info')
    if (question.multipleAnswerMode) multipleAnswerModeQuestion.checked = true
    multipleAnswerModeQuestion.onclick = () => { question.multipleAnswerMode = multipleAnswerModeQuestion.checked; qcm.save() }
    div.appendChild(multipleAnswerModeQuestion)
    div.appendChild(utils.DOM.createSwitchLabelButton('multipleAnswerModeQuestion' + String(index), 'Réponse multiple'))
    /* chronometer mode */
    const divQuestionTimer = utils.DOM.create('div', { class: 'field is-inline-block mx-2 is-vertical-aligned' })
    const pQuestionTimer = utils.DOM.create('p', { class: 'control has-icons-left has-icons-right' })
    const questionTimer = utils.DOM.createInput({ type: 'number', value: String(question.duration), class: 'input has-text-right' })
    if (question.duration === 0) questionTimer.classList.add('has-text-grey-light')
    pQuestionTimer.appendChild(questionTimer)
    divQuestionTimer.appendChild(pQuestionTimer)
    const questionTimerIcon = utils.DOM.create('span', { class: 'icon is-left' })
    questionTimerIcon.innerHTML = '<i class="ri-timer-line"></i>'
    pQuestionTimer.appendChild(questionTimerIcon)
    const questionTimerUnit = utils.DOM.create('span', { class: 'icon is-right' })
    questionTimerUnit.innerHTML = 's.'
    pQuestionTimer.appendChild(questionTimerUnit)
    questionTimer.min = '0'
    questionTimer.max = '360'
    questionTimer.title = '0 = illimité'
    const timeOutSaveTime = 1200
    questionTimer.oninput = () => {
      question.duration = Number(questionTimer.value)
      if (question.duration === 0) {
        questionTimer.classList.add('has-text-grey-light')
        question.timedMode = false
      } else {
        questionTimer.classList.remove('has-text-grey-light')
        question.timedMode = true
      }
      if (timeOutSaveTimer !== undefined) {
        clearTimeout(timeOutSaveTimer)
        timeOutSaveTimer = undefined
      } else {
        timeOutSaveTimer = setTimeout(() => {
          question.save()
        }, timeOutSaveTime)
      }
    }
    div.appendChild(divQuestionTimer)
    /* zoom buttons */
    const zoomIn = utils.DOM.createButton({ class: 'button is-white px-1', html: '<i class="ri-zoom-in-line is-size-5"></i>' })
    const timeOutSaveSize = 2500
    zoomIn.onclick = () => {
      question.zoomIn()
      questionContainer.style.fontSize = String(question.textSize) + 'em'
      if (timeOutTextSizer !== undefined) {
        timeOutTextSizer = undefined
      }
      timeOutTextSizer = setTimeout(() => {
        question.save()
      }, timeOutSaveSize)
    }
    const zoom1 = utils.DOM.createButton({ text: '1:1', class: 'button is-white px-1' })
    zoom1.onclick = () => {
      question.textSize = 1
      questionContainer.style.fontSize = '1em'
      if (timeOutTextSizer !== undefined) {
        timeOutTextSizer = undefined
      }
      timeOutTextSizer = setTimeout(() => {
        question.save()
      }, timeOutSaveSize)
    }
    const zoomOut = utils.DOM.createButton({ class: 'button is-white px-1', html: '<i class="ri-zoom-out-line is-size-5"></i>' })
    zoomOut.onclick = () => {
      question.zoomOut()
      questionContainer.style.fontSize = String(question.textSize) + 'em'
      if (timeOutTextSizer !== undefined) {
        timeOutTextSizer = undefined
      }
      timeOutTextSizer = setTimeout(() => {
        question.save()
      }, timeOutSaveSize)
    }
    div.appendChild(zoomOut)
    div.appendChild(zoom1)
    div.appendChild(zoomIn)
    /* div inclusion */
    const divDropDownMoveActions = utils.DOM.create('div', { class: 'dropdown ml-3 is-float-right' })
    div.appendChild(divDropDownMoveActions)
    const divDropDownTrigger2 = utils.DOM.create('div', { class: 'dropdown-trigger' })
    divDropDownMoveActions.appendChild(divDropDownTrigger2)
    const buttonDropDownTrigger2 = utils.DOM.createButton({ class: 'button' })
    buttonDropDownTrigger2.setAttribute('aria-haspopup', 'true')
    buttonDropDownTrigger2.setAttribute('aria-controls', 'dropdown-menu')
    buttonDropDownTrigger2.appendChild(utils.DOM.create('span', { text: 'Déplacer' }))
    buttonDropDownTrigger2.appendChild(utils.DOM.create('span', { class: 'icon is-small' }).appendChild(utils.DOM.create('i', { class: 'ri-arrow-down-s-line' })))
    buttonDropDownTrigger2.onclick = () => {
      const hisClass = divDropDownMoveActions.classList
      const isActive = hisClass.contains('is-active')
      document.querySelectorAll('#questions-list-container .dropdown.is-active').forEach(el => { el.classList.remove('is-active') })
      if (!isActive) divDropDownMoveActions.classList.add('is-active')
    }
    divDropDownTrigger2.appendChild(buttonDropDownTrigger2)
    const divDropDownMenu2 = utils.DOM.create('div', { class: 'dropdown-menu', id: 'questionMove-' + String(index) })
    divDropDownMenu2.setAttribute('role', 'menu')
    divDropDownMoveActions.appendChild(divDropDownMenu2)
    const divDropDownContent2 = utils.DOM.create('div', { class: 'dropdown-content' })
    divDropDownMenu2.appendChild(divDropDownContent2)
    const questionId = index
    if (index > 0) {
      const buttonMoveToTop = utils.DOM.createAnchor({ class: 'dropdown-item' })
      const itop = utils.DOM.create('i', { class: 'ri-skip-up-line' })
      buttonMoveToTop.appendChild(itop)
      buttonMoveToTop.appendChild(document.createTextNode(' en premier'))
      buttonMoveToTop.onclick = () => {
        qcm.moveQuestionOnTop(questionId)
        qcm.save()
        if (qcmId !== undefined) {
          qcms.list[qcmId][2] = qcm.questionsList[0]
          qcms.save()
        }
        const thisQuestion = currentQuestions[index]
        currentQuestions.splice(index, 1)
        currentQuestions.splice(0, 0, thisQuestion)
        this.refreshQuestions(qcm, qcms, name)
        // this.QCMQuestionsList(name, qcm, qcms, index)
      }
      divDropDownContent2.appendChild(buttonMoveToTop)
      const buttonMoveUp = utils.DOM.createAnchor({ class: 'dropdown-item' })
      const iup = utils.DOM.create('i', { class: 'ri-arrow-up-s-line' })
      buttonMoveUp.appendChild(iup)
      buttonMoveUp.appendChild(document.createTextNode(' monter'))
      buttonMoveUp.onclick = () => {
        qcm.moveQuestionUp(questionId)
        qcm.save()
        if (index === 1 && qcmId !== undefined) {
          qcms.list[qcmId][2] = qcm.questionsList[0]
          qcms.save()
        }
        const thisQuestion = currentQuestions[index]
        currentQuestions.splice(index, 1)
        currentQuestions.splice(index - 1, 0, thisQuestion)
        this.refreshQuestions(qcm, qcms, name)
      }
      divDropDownContent2.appendChild(buttonMoveUp)
    }
    if (index < qcm.questionsList.length - 1) {
      const buttonMoveDown = utils.DOM.createAnchor({ class: 'dropdown-item' })
      const idown = utils.DOM.create('i', { class: 'ri-arrow-down-s-line' })
      buttonMoveDown.appendChild(idown)
      buttonMoveDown.appendChild(document.createTextNode(' descendre'))
      buttonMoveDown.onclick = () => {
        qcm.moveQuestionDown(questionId)
        qcm.save()
        if (index === 0 && qcmId !== undefined) {
          qcms.list[qcmId][2] = qcm.questionsList[0]
          qcms.save()
        }
        const thisQuestion = currentQuestions[index]
        currentQuestions.splice(index, 1)
        currentQuestions.splice(index + 1, 0, thisQuestion)
        this.refreshQuestions(qcm, qcms, name)
      }
      divDropDownContent2.appendChild(buttonMoveDown)
      const buttonMoveToBottom = utils.DOM.createAnchor({ class: 'dropdown-item' })
      const ibottom = utils.DOM.create('i', { class: 'ri-skip-down-line' })
      buttonMoveToBottom.appendChild(ibottom)
      buttonMoveToBottom.appendChild(document.createTextNode(' en dernier'))
      buttonMoveToBottom.onclick = () => {
        qcm.moveQuestionToBottom(questionId)
        qcm.save()
        if (index === 0 && qcmId !== undefined) {
          qcms.list[qcmId][2] = qcm.questionsList[0]
          qcms.save()
        }
        const thisQuestion = currentQuestions[index]
        currentQuestions.splice(index, 1)
        currentQuestions.push(thisQuestion)
        this.refreshQuestions(qcm, qcms, name)
      }
      divDropDownContent2.appendChild(buttonMoveToBottom)
    }
    questionCommands.appendChild(div)
    const questionHeader = utils.DOM.create('header', { class: 'question-content ' + question.type })
    const $divHeader = utils.DOM.create('div')
    questionHeader.appendChild($divHeader)
    $divHeader.innerHTML = question.content
    const { default: InlineEditor } = await import('../js/ckeditor5/build/ckeditor')
    InlineEditor.create($divHeader).then(
      (editor: InlineEditor) => {
        this.inlineEditors.push(editor)
        editor.editing.view.document.on('blur', () => {
          if (question.content !== editor.getData()) {
            question.content = editor.getData()
            question.save()
          }
        })
      }
    ).catch((error: any) => { console.error('Pb création éditeur', error) })
    const answersContainer = utils.DOM.create('ol', { class: 'answers ' + question.type + ' is-relative' }) as HTMLOListElement
    const addAnswerButton = utils.DOM.create('div', { class: 'has-text-centered pointer is-hidden btn-add-answer', title: 'Ajouter une reponse' })
    addAnswerButton.innerHTML = '<i class="ri-play-list-add-line"></i>'
    addAnswerButton.onclick = async () => {
      const answerDestroy = utils.DOM.create('div', { class: 'absolute-left is-clickable answer-destroy' })
      answerDestroy.innerHTML = '<i class="ri-delete-bin-6-line"></i>'
      answerDestroy.onclick = () => {
        const list = [...Array.from(answersContainer.children)]
        const indexOfAnswer = list.indexOf(answerContent)
        question.removeAnswer(indexOfAnswer)
        answersContainer.removeChild(answerContent)
        answersList.splice(indexOfAnswer, 1)
        question.save()
        if (list.length === 4) {
          addAnswerButton.classList.remove('is-hidden')
        }
      }
      const newAnswer = question.addAnwser('')
      answersList.push(newAnswer)
      const answerContent = utils.DOM.create('li', { class: 'answer ' + question.type })
      const $divAnswerContent = utils.DOM.create('div')
      answerContent.appendChild($divAnswerContent)
      $divAnswerContent.innerHTML = newAnswer.content
      const answerIsGood = utils.DOM.create('div', { class: 'absolute-left is-clickable answer-is-good' })
      answerIsGood.innerHTML = '<i class="ri-checkbox-line"></i>'
      answerIsGood.onclick = async () => {
        if (!question.multipleAnswerMode) {
          for (const localanswer of question.answers) {
            if (localanswer !== newAnswer) {
              localanswer.isCorrect = false
            }
          }
          document.querySelectorAll('#questionInList' + String(index) + ' li.answer').forEach(el => { el.classList.remove('is-correct') })
        }
        newAnswer.isCorrect = !newAnswer.isCorrect
        if (newAnswer.isCorrect) answerContent.classList.add('is-correct')
        else answerContent.classList.remove('is-correct')
        question.save()
      }
      const { default: InlineEditor } = await import('../js/ckeditor5/build/ckeditor')
      InlineEditor.create($divAnswerContent).then(
        (editor: InlineEditor) => {
          this.inlineEditors.push(editor)
          editor.editing.view.document.on('blur', () => {
            if (newAnswer.content !== editor.getData()) {
              newAnswer.content = editor.getData()
              question.save()
            }
          })
        })
        .catch((error: any) => { console.error('Problème initialisation éditeur.', error) })
      answersContainer.appendChild(answerContent)
      answerContent.appendChild(answerIsGood)
      answerContent.appendChild(answerDestroy)
      if (answersList.length < 4) {
        addAnswerButton.classList.remove('is-hidden')
      } else {
        addAnswerButton.classList.add('is-hidden')
      }
    }
    const answersList = [...question.answers]
    if (answersList.length < 4) {
      addAnswerButton.classList.remove('is-hidden')
    }
    for (const answer of answersList) {
      const answerContent = utils.DOM.create('li', { class: 'answer ' + question.type })
      const $divAnswerContent = utils.DOM.create('div')
      answerContent.appendChild($divAnswerContent)
      $divAnswerContent.innerHTML = answer.content
      const answerDestroy = utils.DOM.create('div', { class: 'absolute-left is-clickable answer-destroy' })
      answerDestroy.innerHTML = '<i class="ri-delete-bin-6-line"></i>'
      answerDestroy.onclick = () => {
        const list = [...Array.from(answersContainer.children)]
        const indexOfAnswer = list.indexOf(answerContent) / 3
        question.removeAnswer(indexOfAnswer)
        answersContainer.removeChild(answerContent)
        answersList.splice(indexOfAnswer, 1)
        question.save()
        if (list.length === 4) {
          addAnswerButton.classList.remove('is-hidden')
        }
      }
      const answerIsGood = utils.DOM.create('div', { class: 'absolute-left is-clickable answer-is-good' })
      answerIsGood.innerHTML = '<i class="ri-checkbox-line"></i>'
      answerIsGood.onclick = () => {
        if (!question.multipleAnswerMode) {
          for (const localanswer of question.answers) {
            if (localanswer !== answer) {
              localanswer.isCorrect = false
            }
          }
          document.querySelectorAll('#questionInList' + String(index) + ' li.answer').forEach(el => { el.classList.remove('is-correct') })
        }
        answer.isCorrect = !answer.isCorrect
        if (answer.isCorrect) answerContent.classList.add('is-correct')
        else answerContent.classList.remove('is-correct')
        question.save()
      }
      if (answer.isCorrect) { answerContent.classList.add('is-correct') }

      const { default: InlineEditor } = await import('../js/ckeditor5/build/ckeditor')
      InlineEditor.create($divAnswerContent).then(
        (editor: InlineEditor) => {
          this.inlineEditors.push(editor)
          editor.editing.view.document.on('blur', () => {
            const textToSave = editor.getData()
            if (answer.content !== textToSave) {
              answer.content = textToSave
              question.save()
            }
          })
        })
        .catch((error: any) => { console.error('Problème initialisation éditeur.', error) })
      answersContainer.appendChild(answerContent)
      answerContent.appendChild(answerIsGood)
      answerContent.appendChild(answerDestroy)
    }
    questionContainer.appendChild(questionHeader)
    questionContainer.appendChild(answersContainer)
    questionContainer.appendChild(addAnswerButton)
    container.appendChild(questionCommands)
    container.appendChild(questionContainer)
    return container
  },
  switchIsQCMColored (toColor) {
    const qcm = get(currentQcm)
    if (qcm === undefined) return
    const colorEditorQCM = utils.DOM.getById('ColorEditorQCM') as HTMLElement
    if (toColor) {
      document.querySelectorAll('#questions-list').forEach(element => { element.classList.add('colored') })
      qcm.coloredAnswers = true
      colorEditorQCM.classList.remove('is-hidden')
      colorEditorQCM.classList.add('is-inline-block')
      qcm.save()
    } else {
      document.querySelectorAll('#questions-list').forEach(element => { element.classList.remove('colored') })
      qcm.coloredAnswers = false
      colorEditorQCM.classList.add('is-hidden')
      colorEditorQCM.classList.remove('is-inline-block')
      qcm.save()
    }
  },
  // eslint-disable-next-line @typescript-eslint/no-misused-promises
  async statisticsDisplay (groups: Group[]) {
    // Affiche les statistiques enregistrées pour le qcm en cours.
    if (votes === undefined) {
      const voteObject = localStorage.getItem('votes')
      if (voteObject !== null) {
        votes = Votes.remake(JSON.parse(voteObject))
      } else {
        console.warn('statisticsDisplay', 'Pas de données à afficher')
        return
      }
    }
    // répartition par question
    if (get(currentQcm) === undefined) {
      const qcmId = votes.qcmId
      if (qcmId !== undefined) {
        const qcm = await Qcm.load(qcmId)
        currentQcm.set(qcm)
        currentQuestions = await qcm.loadQuestions()
        if (currentQuestions.length === 0) {
          console.warn('statisticsDisplay', 'Problème de récupération des questions')
          return
        }
      } else {
        console.warn('statisticsDisplay', 'Pas de dernier Qcm')
        return
      }
    }
    const $container = utils.DOM.getById('stats-container')
    if ($container !== null) {
      $container.innerHTML = ''
      for (const [index, question] of currentQuestions.entries()) {
        this.questionStatistics(index, $container, question)
      }
      display.renderLatex('stats-container')
    } else {
      console.warn('statisticsDisplay', 'Pas de container pour afficher les stats')
    }
    // affichage des scores des participants
    const groupId = votes.groupId
    const $container2 = utils.DOM.getById('scores-container')
    if (groupId !== undefined && $container2 !== null) {
      const theGroup = Group.getGroupById(groupId, groups)
      if (theGroup !== undefined) {
        this.groupStatistics($container2, theGroup, votes, currentQuestions)
      }
    }
  },
  answersStatistics (votes, index, targetDiv): void {
    const stats = votes.getAnswersOfQuestion(index - 1)
    if (utils.object.getLength(stats) > 0) {
      let total = 0
      const colors = ['#3e95cd', '#3e95cd', '#3e95cd', '#3e95cd']
      if (stats.A !== undefined) total += Number(stats.A)
      if (stats.B !== undefined) total += Number(stats.B)
      if (stats.C !== undefined) total += Number(stats.C)
      if (stats.D !== undefined) total += Number(stats.D)
      if (stats.A !== undefined) {
        const percent = Math.round((Number(stats.A) / total) * 100)
        const li = targetDiv.querySelector('li.answer:nth-child(1)') as HTMLLIElement
        if (li !== null) {
          this.appendStatBar(li, colors[0], percent)
        }
      }
      if (stats.B !== undefined) {
        const percent = Math.round((Number(stats.B) / total) * 100)
        const li = targetDiv.querySelector('li.answer:nth-child(2)') as HTMLLIElement
        if (li !== null) {
          this.appendStatBar(li, colors[1], percent)
        }
      }
      if (stats.C !== undefined) {
        const percent = Math.round((Number(stats.C) / total) * 100)
        const li = targetDiv.querySelector('li.answer:nth-child(3)') as HTMLLIElement
        if (li !== null) {
          this.appendStatBar(li, colors[2], percent)
        }
      }
      if (stats.D !== undefined) {
        const percent = Math.round((Number(stats.D) / total) * 100)
        const li = targetDiv.querySelector('li.answer:nth-child(4)') as HTMLLIElement
        if (li !== null) {
          this.appendStatBar(li, colors[3], percent)
        }
      }
    }
  },
  appendStatBar (li, color, percent): void {
    const child = li.querySelector('.statsbar')
    if (child !== null) {
      li.removeChild(child)
    }
    const statsBar = utils.DOM.create('div')
    statsBar.className = 'statsbar'
    const percentBar = utils.DOM.create('div')
    percentBar.className = 'percentbar'
    percentBar.style.width = String(percent) + '%'
    percentBar.style.backgroundColor = color
    statsBar.appendChild(percentBar)
    li.appendChild(statsBar)
  },
  questionStatistics (index, container, question?): void {
    // Affiche les stats pour la question en cours
    // récupère les réponses fournies pour la question
    const stats = votes.getAnswersOfQuestion(index)
    if (utils.object.getLength(stats) > 0) {
      if (container !== null) {
        const article = utils.DOM.create('article', { class: 'cell has-text-centered' })
        if (question !== undefined) {
          const divEnonce = utils.DOM.create('div')
          divEnonce.innerHTML = question.content
          article.appendChild(divEnonce)
        }
        const canvasContainer = utils.DOM.create('div', { class: 'square' })
        const canvasStats = utils.DOM.createCanvas()
        canvasContainer.appendChild(canvasStats)
        article.appendChild(canvasContainer)
        container.appendChild(article)
        const colors = ['#3e95cd', '#3e95cd', '#3e95cd', '#3e95cd']
        const colorsGoodAnswers = ['#3e95cd', '#3e95cd', '#3e95cd', '#3e95cd']
        let count = 0
        if (question === undefined) question = currentQuestions[currentQuestionIndex - 1]
        question?.answers.forEach(answer => {
          if (answer.isCorrect) {
            colorsGoodAnswers[count] = '#adff2f'
          }
          count++
        })
        const data = {
          type: 'bar',
          data: {
            labels: ['A', 'B', 'C', 'D'],
            datasets: [{
              label: 'Réponses question ' + String(index + 1),
              backgroundColor: colors,
              data: [
                stats.A !== undefined ? stats.A : 0,
                stats.B !== undefined ? stats.B : 0,
                stats.C !== undefined ? stats.C : 0,
                stats.D !== undefined ? stats.D : 0
              ]
            }]
          },
          options: {
            responsive: true,
            indexAxis: 'y'
          }
        }
        // eslint-disable-next-line no-undef
        const chart = new Chart(canvasStats, data)
        if (chart === null) console.warn('Impossible de créer le graphique.', index + 1, question)
        const buttonShowGood = utils.DOM.createButton({ text: 'Révéler', class: 'button' })
        article.appendChild(buttonShowGood)
        buttonShowGood.onclick = () => {
          if (chart !== null) {
            chart.data.datasets[0].backgroundColor = colorsGoodAnswers
            chart.update()
          }
        }
      }
    } else {
      console.warn('questionStatistics', 'Pas de stats à afficher')
    }
  },
  groupStatistics (container, group, votes, questions): void {
    container.innerHTML = ''
    // Affiche les stats pour le groupe
    const table = utils.DOM.create('table')
    container.appendChild(table)
    const tr = utils.DOM.create('tr')
    table.appendChild(tr)
    const th = utils.DOM.create('th', { text: 'Participant' })
    tr.appendChild(th)
    // colonnes pour les questions
    for (let id = 1; id <= questions.length; id++) {
      const th3 = utils.DOM.create('th', { text: 'Q' + String(id) })
      tr.appendChild(th3)
    }
    const th4 = utils.DOM.create('th', { text: 'Scores' })
    tr.appendChild(th4)
    for (const key in group.students) {
      table.appendChild(this.studentStatistics(group.students[key], votes, questions))
    }
    container.appendChild(table)
  },
  studentStatistics (student, votes, questions): HTMLElement {
    const tr = utils.DOM.create('tr')
    const td = utils.DOM.create('td', { text: student.firstname + ' ' + student.name })
    tr.appendChild(td)
    let score = 0
    for (let id = 0; id < questions.length; id++) {
      const td2 = utils.DOM.create('td')
      if (votes.markersAnswers[student.markerId]?.[id] !== undefined) {
        const goodAnswer = utils.numberToLetter(questions[id].getGoodAnswer()).join(', ')
        const answer = votes.markersAnswers[student.markerId][id].value.sort().join(', ')
        if (answer === goodAnswer) {
          score++
          td2.classList.add('good')
        }
        td2.innerHTML = answer
      } else {
        td2.innerHTML = '-'
      }
      tr.appendChild(td2)
    }
    const td3 = utils.DOM.create('td', { text: String(score) + '/' + String(questions.length) })
    tr.appendChild(td3)
    return tr
  },
  showGoodAnswersOfStudents (): void {
    if (get(currentGroup) === undefined) return
    for (const id in votes.markersAnswers) {
      if (votes.markersAnswers[id][currentQuestionIndex - 1] !== undefined) {
        const goodAnswers = utils.numberToLetter(currentQuestions[currentQuestionIndex - 1].getGoodAnswer())
        if (utils.array.areEqual(votes.markersAnswers[id][currentQuestionIndex - 1].value, goodAnswers)) {
          utils.DOM.addClass('marker' + id, 'hasgood')
        }
      }
    }
  },
  displayStatsOfAnswers (second = ''): void {
    let target = '#qcm-container' + second
    let thisvotes, currentQ
    if (second === '') {
      target += ' .question:nth-child(' + String(currentQuestionIndex) + ')'
      thisvotes = votes
      currentQ = currentQuestionIndex
    } else {
      target += ' .question:nth-child(' + String(currentQuestionIndex2) + ')'
      thisvotes = votes2
      currentQ = currentQuestionIndex2
    }
    const targetDiv = document.querySelector(target) as HTMLDivElement
    if (targetDiv !== null) {
      this.answersStatistics(thisvotes, currentQ, targetDiv)
    }
  },
  emptyQCMQuestionsList (): void {
    const questionListName = utils.DOM.getById('question-list-name')
    if (questionListName !== null) {
      questionListName.innerHTML = ''
      questionListName.appendChild(document.createTextNode('Sélectionner un QCM...'))
    }
    const dom = utils.DOM.getById('questions-list')
    if (dom !== null) {
      dom.innerHTML = `<div class="py-6 is-size-3 has-text-grey-light has-text-weight-bold is-flex">
      <div class="is-flex is-justify-content-center is-flex-direction-column mx-4"><i
          class="ri-arrow-left-s-line"></i></div>
      <div>Sélectionner/Créer/Charger un qcm<br> pour voir et éditer ses questions</div>
    </div>`
    }
  },
  QCMDelete (qcms: QcmList) {
    if (confirm('Vous êtes sur le point de supprimer le QCM.\nConfirmer ?')) {
      const editorQCMName = utils.DOM.getById('editor-qcm-name')
      if (editorQCMName !== null) {
        const qcmID = editorQCMName.dataset.id
        const qcm = get(currentQcm)
        if (qcm !== undefined && qcmID === qcm.id) {
          qcms.deleteQcmById(qcmID).then(() => {
            qcms.save()
            this.emptyQCMQuestionsList()
            document.getElementById('btn-qcm')?.click()
          }).catch(err => { console.error('Erreur de suppression de QCM', err) })
        }
      }
    }
  },
  async refreshCamerasList ($container?): Promise<void> {
    if ($container === undefined) {
      $container = utils.DOM.getById('selectCameraChoice') as HTMLSelectElement
    }
    await camera.detect().then((data) => {
      if ($container !== null) {
        $container.innerHTML = '<option value="-1">Pas de caméra</option>'
        if (typeof data !== 'string') {
          data.forEach(element => {
            const option = utils.DOM.createOption({ text: element[1], value: String(element[2]) })
            $container.appendChild(option)
          })
        } else {
          alert(data)
        }
        $container.onchange = () => {
          camera.setSource(Number($container.value))
          const $cameraVideo = document.getElementById('videotest') as HTMLVideoElement
          if ($cameraVideo !== null) {
            camera.start($cameraVideo)
          }
        }
      }
    })
  },
  hideQCMCamShow () {
    utils.DOM.addClass('QCMCamShow', 'is-hidden')
    const qcmContainer = utils.DOM.getById('qcm-container')
    if (qcmContainer !== null) qcmContainer.innerHTML = ''
  },
  startDetection (second = ''): void {
    // TODO : gérer le second
    let timeZero = Infinity
    let thisQuestion = currentQuestions[currentQuestionIndex - 1]
    if (second === '0') {
      thisQuestion = currentQuestions2[currentQuestionIndex2 - 1]
      votes.setQuestion(currentQuestionIndex2 - 1, thisQuestion)
    } else {
      votes.setQuestion(currentQuestionIndex - 1, thisQuestion)
    }
    if (thisQuestion.timedMode) {
      const $divChrono = document.querySelector('#qcm-container' + second + ' .chrono') as HTMLDivElement
      if ($divChrono !== null) {
        $divChrono.style.width = '100%'
      } else {
        const $divChronoContainer = utils.DOM.create('section', { class: 'chrono-container' })
        const $divChrono = utils.DOM.create('article', { class: 'chrono' })
        $divChronoContainer.appendChild($divChrono)
        document.getElementById('qcm-container' + second)?.prepend($divChronoContainer)
      }
    }
    utils.DOM.removeClass('cameraCopy', 'inactiv')
    if (tictoc !== null) {
      clearInterval(tictoc)
      tictoc = null
    }
    setTimeout(() => {
      timeZero = Date.now()
      tictoc = setInterval(() => {
        if (thisQuestion.timedMode) {
          const $divChrono = document.querySelector('#qcm-container' + second + ' .chrono') as HTMLDivElement
          if (Date.now() - timeZero > thisQuestion.duration * 1000) {
            if ($divChrono !== null) {
              $divChrono.style.width = '0'
            }
            this.stopDetection(second)
            return
          } else {
            if ($divChrono !== null) {
              const timeLeft = Math.floor((thisQuestion.duration * 1000 - (Date.now() - timeZero)) * 100 / (thisQuestion.duration * 1000))
              $divChrono.style.width = String(timeLeft) + '%'
            }
          }
        }
        const mks: Record<number, string> | undefined = detection.detect()
        if (utils.object.getLength(mks) > 0 && mks !== undefined) {
          votes.analyseAnswers(mks, config.changeOpinion)
          this.slideMarkerSetAnswerState()
        }
      }, 97)
    }, 500)
  },
  startCountDown (second = ''): void {
    let timeZero = Infinity
    let thisQuestion = currentQuestions[currentQuestionIndex - 1]
    if (second === '0') {
      thisQuestion = currentQuestions2[currentQuestionIndex2 - 1]
    }
    if (!thisQuestion.timedMode) {
      return
    }
    const $divChrono = document.querySelector('#qcm-container' + second + ' .chrono') as HTMLDivElement
    if ($divChrono !== null) {
      $divChrono.style.width = '100%'
    } else {
      const $divChronoContainer = utils.DOM.create('section', { class: 'chrono-container' })
      const $divChrono = utils.DOM.create('article', { class: 'chrono' })
      $divChronoContainer.appendChild($divChrono)
      document.getElementById('qcm-container' + second)?.prepend($divChronoContainer)
    }
    if (tictoc !== null) {
      clearInterval(tictoc)
      tictoc = null
    }
    setTimeout(() => {
      timeZero = Date.now()
      tictoc = setInterval(() => {
        const $divChrono = document.querySelector('#qcm-container' + second + ' .chrono') as HTMLDivElement
        if (Date.now() - timeZero > thisQuestion.duration * 1000) {
          if ($divChrono !== null) {
            $divChrono.style.width = '0'
          }
          if (tictoc !== null) {
            clearInterval(tictoc)
            tictoc = null
          }
          this.nextQuestion(second)
        } else {
          if ($divChrono !== null) {
            const timeLeft = Math.floor((thisQuestion.duration * 1000 - (Date.now() - timeZero)) * 100 / (thisQuestion.duration * 1000))
            $divChrono.style.width = String(timeLeft) + '%'
          }
        }
      }, 97)
    }, 1000)
  },
  startCallRoll (group1: Group, group2?: Group) {
    if (tictoc !== null) {
      clearInterval(tictoc)
      tictoc = null
    }
    tictoc = setInterval(() => {
      const mks: Record<number, string> | undefined = detection.detect()
      if (utils.object.getLength(mks) > 0 && mks !== undefined) {
        group1.getPresents(mks)
        if (group2 !== undefined) group2.getPresents(mks)
        this.callRollSetPresentState(group1, group2)
      }
    }, 97)
  },
  stopCallRoll () {
    if (tictoc !== null) {
      clearInterval(tictoc)
      tictoc = null
    }
  },
  stopDetection (second = ''): boolean {
    if (!detection.isStarted()) {
      this.startDetection(second)
      return false
    }
    detection.stop()
    utils.DOM.addClass('cameraCopy', 'inactiv')
    if (tictoc !== null) clearInterval(tictoc)
    tictoc = null
    localStorage.setItem('votes', JSON.stringify(votes))
    return true
  },
  callRoll (group1: Group, group2?: Group) {
    utils.DOM.removeClass('qm-diaporama-selection', 'is-active')
    utils.DOM.addClass('qm-callroll', 'is-active')
    const $presentContainer = utils.DOM.getById('presents-container')
    const $absentContainer = utils.DOM.getById('absents-container')
    if ($presentContainer !== null && $absentContainer !== null) {
      $presentContainer.innerHTML = ''
      $absentContainer.innerHTML = ''
    }
    if (group1 !== undefined) {
      this.callRollPopulate(group1)
    }
    if (group2 !== undefined) {
      this.callRollPopulate(group2)
    }
    this.startCallRoll(group1, group2)
  },
  callRollPopulate (group: Group) {
    const $container = utils.DOM.getById('absents-container')
    if ($container !== null) {
      $container.innerHTML = ''
      for (const key in group.students) {
        const student = group.students[key]
        student.present = false
        const $containerstudent = utils.DOM.create('article', { class: 'student', id: 'student' + String(student.markerId) })
        $containerstudent.dataset.markerId = String(student.markerId)
        const $number = utils.DOM.create('div', { text: String(student.markerId), class: 'markerid' })
        const $div = utils.DOM.create('div', { class: 'studentname', text: ((student.firstname ?? '') + ' ' + (student.name).trim()) })
        $containerstudent.appendChild($number)
        $containerstudent.appendChild($div)
        $container.appendChild($containerstudent)
      }
    }
  },
  callRollSetPresentState (group1: Group, group2?: Group) {
    // changer les participants présents de place
    const $presentContainer = utils.DOM.getById('presents-container')
    const $absentContainer = utils.DOM.getById('absents-container')
    if ($presentContainer !== null && $absentContainer !== null) {
      if ($absentContainer.children.length > 0) {
        for (const absent of Array.from($absentContainer.children)) {
          for (const key in group1.students) {
            const student = group1.students[key]
            if ((absent as HTMLElement).dataset.markerId === String(student.markerId) && student.present) {
              $presentContainer.appendChild(absent)
            }
          }
        }
        if (group2 !== undefined) {
          for (const absent of Array.from($absentContainer.children)) {
            for (const key in group2.students) {
              const student = group2.students[key]
              if ((absent as HTMLElement).dataset.markerId === String(student.markerId) && student.present) {
                $presentContainer.appendChild(absent)
              }
            }
          }
        }
      } else {
        this.stopCallRoll()
      }
    }
  },
  slidePopulate (questions: Question[], second = '', addCache = false, isColored = false) {
    const qcmContainer = utils.DOM.getById('qcm-container' + second)
    if (qcmContainer !== null) {
      qcmContainer.innerHTML = ''
      if (isColored) {
        qcmContainer.classList.add('colored')
      } else {
        qcmContainer.classList.remove('colored')
      }
    }
    if (addCache) {
      this.slideAddCache(second)
    }
    for (const [index, question] of questions.entries()) {
      let defaultClass = 'question minus100'
      if (index === 0) defaultClass = 'question slidein'
      const questionContainer = utils.DOM.create('div', { id: 'questionContainer' + String(index), class: defaultClass })
      if (!isNaN(question.textSize)) questionContainer.style.fontSize = String(question.textSize) + 'em'
      questionContainer.classList.add(question.displayMode)
      const questionHeader = utils.DOM.create('header', { class: 'question-content ' + question.type })
      const $divHeader = utils.DOM.create('div')
      $divHeader.innerHTML = question.content
      questionHeader.appendChild($divHeader)
      const answersContainer = utils.DOM.create('ol', { class: 'answers ' + question.type }) as HTMLOListElement
      const answersList = [...question.answers]
      if (question.shuffleMode) utils.array.shuffle(answersList)
      for (const answer of answersList) {
        const answerContent = utils.DOM.create('li', { class: 'answer ' + question.type })
        if (answer.content === '') answerContent.innerHTML = '<p>&nbsp;</p>'
        else answerContent.innerHTML = answer.content
        if (answer.isCorrect) { answerContent.classList.add('is-correct') }
        answersContainer.appendChild(answerContent)
      }
      questionContainer.appendChild(questionHeader)
      questionContainer.appendChild(answersContainer)
      qcmContainer?.appendChild(questionContainer)
    }
    this.renderLatex('qcm-container' + second)
    const fin = utils.DOM.create('div', { id: 'slideEnd' + second, class: 'question fin minus100' })
    fin.innerHTML = 'FIN'
    qcmContainer?.appendChild(fin)
    this.setQCMQuestionFontSize()
  },
  slideAddCache (second = '') {
    const qcmContainer = utils.DOM.getById('qcm-container' + second)
    const cacheIsActiv = document.querySelector('#qcm-container' + second + ' .qcm-cache')
    if (cacheIsActiv !== null) {
      qcmContainer?.removeChild(cacheIsActiv)
      // on démarre la détection quand on enlève le cache
      this.startDetection(second)
      return
    }
    const divCache = utils.DOM.create('div', { id: 'qcm-cache' + second, class: 'qcm-cache' })
    divCache.innerHTML = '<div><i class="ri-play-circle-line ri-5x"></i></div>'
    divCache.onclick = () => { qcmContainer?.removeChild(divCache) }
    qcmContainer?.appendChild(divCache)
  },
  slideGroupPopulate (group: Group, second = '') {
    const $groupContainer = utils.DOM.getById('participant-list' + second)
    const $absentContainer = utils.DOM.create('div', { id: 'absents-list' + second })
    if ($groupContainer !== null) {
      $groupContainer.innerHTML = ''
      currentGroup.set(group)
      const studentsIdsByMarkers = group.orderedByMarkersIdStudentsIds
      for (const key of studentsIdsByMarkers) {
        const student = group.students[key]
        const studentContainer = utils.DOM.create('article', { id: 'studentContainer' + String(key), class: 'student' })
        const studentMarker = utils.DOM.create('div', { id: 'marker' + String(student.markerId), class: 'markerid', text: String(student.markerId) })
        const studentName = utils.DOM.create('div', { class: 'studentname', text: (student.firstname + ' ' + student.name).trim() })
        studentContainer.appendChild(studentMarker)
        studentContainer.appendChild(studentName)
        if (student.present) {
          $groupContainer.appendChild(studentContainer)
        } else {
          $absentContainer.appendChild(studentContainer)
        }
      }
      if ($absentContainer.children.length > 0) {
        const $header = utils.DOM.create('h3', { text: 'Absents' })
        $absentContainer.prepend($header)
        $groupContainer.appendChild($absentContainer)
      }
    }
  },
  showNamesOfStudents (second = ''): void {
    document.getElementById('participant-list' + String(second))?.classList.toggle('onlyNumbers')
  },
  showAnswersOfStudents (second = ''): void {
    const list = Array.from(document.querySelectorAll('#participant-list' + String(second) + ' .student'))
    for (const el of list) {
      continue
    }
  },
  slideMarkersReset (second = ''): void {
    document.querySelectorAll('#participant-list' + second + ' .markerid').forEach(el => {
      el.className = 'markerid'
    })
  },
  slideMarkerSetAnswerState () {
    for (const id in votes.markersAnswers) {
      const answer = votes.markersAnswers[id][currentQuestionIndex - 1]
      if (answer !== undefined) {
        const lastVote = answer.history[answer.history.length - 1]
        if (lastVote === undefined) continue
        const marker = utils.DOM.getById('marker' + id)
        if (marker !== null) {
          if (votes.markersAnswers[id][currentQuestionIndex - 1].value.length > 0) {
            utils.DOM.addClass('marker' + id, 'hasVoted')
          }
          if (answer.history.length > 1 && marker.dataset.vote !== lastVote[0]) {
            let classVote = ('addVote')
            if (lastVote[1] === '-') classVote = 'removeVote'
            utils.DOM.addClass('marker' + id, classVote)
            setTimeout(() => { utils.DOM.removeClass('marker' + id, classVote) }, 3000)
          }
          marker.dataset.vote = lastVote[0]
        }
      }
    }
  },
  nextQuestion (second = '') {
    let qcm, current, numero
    if (second === '') {
      qcm = get(currentQcm)
      current = currentQuestionIndex
      numero = document.querySelectorAll('.qcm-numero')
    } else {
      qcm = get(currentQcm2)
      current = currentQuestionIndex2
      numero = document.querySelectorAll('#qcm-numero0')
    }
    if (qcm !== undefined) {
      const total = qcm.questionsList.length
      if (total > current) {
        const slide0 = document.querySelector('#qcm-container' + second + ' .question:nth-of-type(' + String(current) + ')')
        if (slide0 !== null) {
          slide0.classList.remove('slidein')
          slide0.classList.add('slideout')
          setTimeout(() => {
            slide0.classList.add('is-hidden')
          }, 500)
        }
        const slide1 = document.querySelector('#qcm-container' + second + ' .question:nth-of-type(' + String(current + 1) + ')')
        if (slide1 !== null) {
          slide1.classList.remove('minus100')
          slide1.classList.add('slidein')
        }
        numero.forEach(el => {
          el.innerHTML = String(Number(current) + 1) + ' / ' + String(total)
        })
        if (second === '') {
          currentQuestionIndex++
        } else {
          currentQuestionIndex2++
        }
        this.slideMarkersReset()
        if (dectectionWithCamera) this.startDetection(second)
        else this.startCountDown(second)
      } else {
        if (dectectionWithCamera) this.stopDetection(second)
        // arrêt de la camera
        const videoElement = utils.DOM.getById('videotest') as HTMLVideoElement
        camera.stop(videoElement)
        const slide0 = document.querySelector('#qcm-container' + second + ' .question:nth-of-type(' + String(current) + ')')
        if (slide0 !== null) {
          slide0.classList.remove('slidein')
          slide0.classList.add('slideout', 'minus100')
          setTimeout(() => {
            slide0.classList.add('is-hidden')
          }, 500)
        }
        const slideEnd = document.getElementById('slideEnd' + second)
        slideEnd?.classList.remove('minus100')
        slideEnd?.classList.add('slidein')
      }
    }
  },
  async startQCM (groups: Group[], rollCallIsOver = false) {
    // réécriture On va utiliser la classe Session
    // liste des groupes sélectionnés ou créés via l'appel
    const mode = utils.forms.getRadioValue('qcm-mode-choice') ?? 'normal'
    const shuffleAnswersMode: boolean[] = [false]
    const shuffleQuestionsMonde: boolean[] = [false]
    const listsOfQCMs: string[][] = [[]]
    const listsOfQuestions: string[][] = [[]]
    const listOfConfigs: Array<Record<string, string[]>> = [{}]
    const listsOfStudents: number[][] = [[]]
    // liste des QCM sélectionnés
    const selectQCM = utils.DOM.getById('select-qcm-choice') as HTMLSelectElement
    if (selectQCM !== null) {
      for (const option of Array.from(selectQCM.options)) {
        if (option.selected && option.value !== '') {
          listsOfQCMs[0].push(option.value)
        }
      }
      // get questions id for each QCM
      for (const qcmId of listsOfQCMs[0]) {
        const qcm = await Qcm.load(qcmId)
        listsOfQuestions[0] = listsOfQuestions[0].concat(qcm.questionsList)
      }
      // check if extract n questions from list
      const haveToExtract = utils.forms.getRadioValue('qcm-slides')
      if (haveToExtract === 'extract') {
        const newList = []
        const copyOfList = listsOfQuestions[0].slice()
        const num = utils.forms.getValue('qcm-slides-number')
        if (num === null || num === '' || num === undefined) {
          console.error('qcm-slides-number not found')
          return
        }
        const number = parseInt(num)
        for (let i = 0; i < number; i++) {
          const random = Math.floor(Math.random() * copyOfList.length)
          newList.push(copyOfList[random])
          copyOfList.splice(random, 1)
        }
        listsOfQuestions[0] = newList
      }
      // shuffle questions
      const orderOfQuestions = utils.forms.getRadioValue('qcm-order-questions')
      if (orderOfQuestions === 'random') {
        shuffleQuestionsMonde[0] = true
        if (haveToExtract !== 'extract') { // si extraction, c'est déjà random
          listsOfQuestions[0].sort(() => Math.random() - 0.5)
        }
      }
      const orderAnswers = utils.forms.getRadioValue('qcm-order-answers')
      // list of shuffled questions
      if (orderAnswers === 'random') {
        shuffleAnswersMode[0] = true
        // on mélange les réponses des questions et on stocke pour l'analyse des scores
        for (const question of listsOfQuestions[0]) {
          const aQuestion = await Question.load(question)
          if (aQuestion.shuffleMode) {
            const conf = ['A', 'B', 'C', 'D'].slice(0, aQuestion.answers.length - 1)
            conf.sort(() => Math.random() - 0.5)
            listOfConfigs[0][question] = conf
          }
        }
      }
    } else {
      alert('Veuillez sélectionner au moins un QCM')
      return
    }
    if (mode === 'double') {
      listsOfQCMs.push([])
      listOfConfigs.push({})
      const selectQCM2 = utils.DOM.getById('select-qcm-choice2') as HTMLSelectElement
      if (selectQCM2 !== null) {
        for (const option of Array.from(selectQCM2.options)) {
          if (option.selected && option.value !== '') {
            listsOfQCMs[1].push(option.value)
          }
        }
      }
      // get questions id for each QCM
      for (const qcmId of listsOfQCMs[1]) {
        const qcm = await Qcm.load(qcmId)
        listsOfQuestions[1] = listsOfQuestions[1].concat(qcm.questionsList)
      }
      // check if extract n questions from list
      const haveToExtract = utils.forms.getRadioValue('qcm-slides0')
      if (haveToExtract === 'extract') {
        const newList = []
        const copyOfList = listsOfQuestions[1].slice()
        const num = utils.forms.getValue('qcm-slides-number0')
        if (num === null || num === '' || num === undefined) {
          console.error('qcm-slides-number not found')
          return
        }
        const number = parseInt(num)
        for (let i = 0; i < number; i++) {
          const random = Math.floor(Math.random() * copyOfList.length)
          newList.push(copyOfList[random])
          copyOfList.splice(random, 1)
        }
        listsOfQuestions[1] = newList
      }
      // shuffle questions
      const orderOfQuestions = utils.forms.getRadioValue('qcm-order-questions0')
      if (orderOfQuestions === 'random') {
        shuffleQuestionsMonde[1] = true
        if (haveToExtract !== 'extract') { // si extraction, c'est déjà random
          listsOfQuestions[1].sort(() => Math.random() - 0.5)
        }
      }
      const orderAnswers = utils.forms.getRadioValue('qcm-order-answers')
      // list of shuffled questions
      if (orderAnswers === 'random') {
        shuffleAnswersMode[1] = true
        // on mélange les réponses des questions et on stocke pour l'analyse des scores
        for (const question of listsOfQuestions[1]) {
          const aQuestion = await Question.load(question)
          if (aQuestion.shuffleMode) {
            const conf = ['A', 'B', 'C', 'D'].slice(0, aQuestion.answers.length - 1)
            conf.sort(() => Math.random() - 0.5)
            listOfConfigs[1][question] = conf
          }
        }
      }
    }
    // listes des élèves
    const selectGroup = utils.DOM.getById('select-group-choice') as HTMLSelectElement
    let group1, group2
    const groupIds = []
    if (selectGroup !== null) {
      for (const option of Array.from(selectGroup.options)) {
        if (option.selected && option.value !== '') {
          groupIds.push(Number(option.value))
        }
      }
      if (groupIds.length > 2) {
        alert('Trop de groupes sélectionnés')
        return
      } else if (mode === 'normal' && groupIds.length > 1) {
        alert('Trop de groupes sélectionnés')
        return
      } else if (mode === 'normal') {
        group1 = Group.getGroupById(Number(groupIds[0]), groups) as Group
        listsOfStudents[0] = Object.keys(group1.students).map((i) => Number(i))
      } else if (mode === 'double' || mode === 'duel') {
        if (groupIds.length < 2) {
          // usage des sous-groupes si existants
          group1 = Group.getGroupById(Number(groupIds[0]), groups) as Group
          if (group1.subgroups[0].length === 0 || group1.subgroups[1].length === 0) {
            alert('Le groupe sélectionné n\'a pas de sous-groupes défini')
            return
          } else {
            listsOfStudents[0] = group1.subgroups[0]
            listsOfStudents[1] = group1.subgroups[1]
          }
        } else if (groupIds.length === 2) {
          group1 = Group.getGroupById(Number(groupIds[0]), groups) as Group
          group2 = Group.getGroupById(Number(groupIds[1]), groups) as Group
          // comparer les numéros de marqueurs des groupes
          let double = false
          for (const key of Object.keys(group1.students)) {
            for (const key2 of Object.keys(group2.students)) {
              if (group1.students[Number(key)].markerId === group2.students[Number(key2)].markerId) {
                double = true
                break
              }
            }
            if (double) {
              alert('Il y a au moins une carte de numéro identique dans les 2 groupes')
              return
            }
          }
          listsOfStudents[0] = Object.keys(group1.students).map((i) => Number(i))
          listsOfStudents[1] = Object.keys(group2.students).map((i) => Number(i))
        }
      }
    }
    const groupsList = [groupIds, listsOfStudents[0], listsOfStudents[1]]
    const questionsList = [[]]
    for (const question of listsOfQuestions[0]) {
      questionsList[0][question] = listOfConfigs[0][question] ?? ''
    }
    if (listsOfQuestions[1] !== undefined) {
      for (const question of listsOfQuestions[1]) {
        questionsList[1][question] = listOfConfigs[1][question] ?? ''
      }
    }
    session = new Session(mode, listsOfQCMs, questionsList, groupsList, shuffleAnswersMode, shuffleQuestionsMonde)
    // démarrage de la détection
    dectectionWithCamera = false
    if (camera.isReady) {
      const videoElement = utils.DOM.getById('videotest') as HTMLVideoElement
      dectectionWithCamera = detection.start(videoElement)
    }
    const howTostart = utils.forms.getRadioValue('qcm-start')
    utils.DOM.removeClass('QCMCamShow', 'double')
    utils.DOM.removeClass('QCMCamShow', 'duel')
    if (howTostart === 'appel' && !rollCallIsOver && group1 !== undefined) {
      display.callRoll(group1, group2)
      console.log('appel')
      return
    }
    currentTabId.set('QCMCamShow')

    display.setStyleSheetColor('destroy')
    if (qcmIds[0] !== undefined) {
      const getqcm = Qcm.load(qcmIds[0])
      // TODO : deux qcm possibles, en parallèle
      if (getqcm !== undefined) {
        getqcm.then(qcm => {
          let addCache = false
          if (howTostart === 'hidden') addCache = true
          currentQcm.set(qcm)
          display.setStyleSheetColor('1')
          currentQuestionIndex = 1
          const isColored = qcm.coloredAnswers
          qcm.loadQuestions().then(
            questions => {
              currentQuestions = questions
              this.slidePopulate(questions, '', addCache, isColored)
              // démarrer la détection ne peut commencer avant
              if (dectectionWithCamera && !addCache) {
                this.startDetection()
              } else if (!addCache) {
                this.startCountDown()
              }
              const numero = document.querySelectorAll('.qcm-numero')
              numero.forEach(el => {
                el.innerHTML = String(1) + ' / ' + String(questions.length)
              })
            }
          ).catch(err => { console.error('Erreur de chargement des questions.', err) })
        }).catch(() => {
          return false
        })
      }
      if (group1 !== undefined && qcmIds[0] !== undefined) {
        votes = new Votes(group1.id, qcmIds[0])
      }
    }
    if (qcmIds[1] !== undefined && group2 !== undefined) {
      utils.DOM.addClass('QCMCamShow', 'double')
      const getqcm = Qcm.load(qcmIds[1])
      if (getqcm !== undefined) {
        getqcm.then(qcm => {
          currentQcm2.set(qcm)
          display.setStyleSheetColor('0')
          currentQuestionIndex2 = 1
          const isColored = qcm.coloredAnswers
          let addCache = false
          if (howTostart === 'hidden') addCache = true
          qcm.loadQuestions().then(
            questions => {
              currentQuestions2 = questions
              this.slidePopulate(questions, '0', addCache, isColored)
              const numero = document.querySelectorAll('#qcm-numero0')
              numero.forEach(el => {
                el.innerHTML = String(1) + ' / ' + String(questions.length)
              })
            }
          ).catch(err => { console.error('Erreur de chargement des questions.', err) })
        }).catch(() => {
          return false
        })
      }
      votes2 = new Votes(group2.id, qcmIds[1])
    } else if (group2 !== undefined) {
      utils.DOM.addClass('QCMCamShow', 'duel')
      votes2 = new Votes(group2.id, qcmIds[0])
    }
    if (group1 !== undefined) {
      this.slideGroupPopulate(group1)
      this.slideMarkersReset()
    }
    if (group2 !== undefined) {
      this.slideGroupPopulate(group2, '0')
      this.slideMarkersReset('0')
    }
    utils.DOM.removeClass('qm-diaporama-selection', 'is-active')
  },
  showGoodAnswer (second = '') {
    let qcmIndex, current
    if (second === '') {
      qcmIndex = currentQuestionIndex - 1
      current = get(currentQcm)
    } else {
      qcmIndex = currentQuestionIndex2 - 1
      current = get(currentQcm2)
    }
    if (current !== undefined && qcmIndex >= 0) {
      const slide = document.querySelector('#qcm-container' + second + ' #questionContainer' + String(qcmIndex))
      if (slide !== null) {
        slide.classList.toggle('showCorrectAnswers')
      }
    }
  },
  renderLatex (eltId: string) {
    const elTex = document.querySelectorAll('#' + eltId + ' .math-tex')
    let Tex = ''
    for (const texElement of Array.from(elTex)) {
      if (texElement.querySelector('span.katex') !== null) {
        const annotation = texElement.querySelector('annotation')
        if (annotation !== null) Tex = utils.text.decodeHtmlEntity(annotation.innerHTML.trim())
      } else {
        Tex = utils.text.decodeHtmlEntity(texElement.innerHTML.trim())
      }
      Tex = Tex.replace(/\\[()]/g, '')
      try {
        katex.render(Tex, texElement, {
          throwOnError: false,
          errorColor: '#FFF'
        })
      } catch (err) {
        // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
        texElement.innerHTML = err + ' erreur de rendu avec ' + Tex
      }
    }
  },
  createSelect (list: string[] | number[], selected?: string | number): HTMLSelectElement {
    const dom = utils.DOM.create('select') as HTMLSelectElement
    for (const value of list) {
      const option = utils.DOM.createOption({ value: String(value), text: String(value) })
      if (value === selected) {
        option.selected = true
      }
      dom.appendChild(option)
    }
    return dom
  },
  createGroupCheckList (student: Student, groups: Group[]): HTMLElement {
    const dom = utils.DOM.create('div', { class: 'group-checklist field' })
    for (const group of groups) {
      const checkbox = utils.DOM.createInput({ type: 'checkbox', id: 'check' + group.name, value: group.name, class: 'is-checkradio' })
      if (student.groups.includes(group.name)) checkbox.checked = true
      const label = utils.DOM.create('label', { text: group.name }) as HTMLLabelElement
      label.htmlFor = 'check' + group.name
      dom.appendChild(checkbox)
      dom.appendChild(label)
      checkbox.onclick = async () => {
        if (checkbox.checked) {
          await student.addToGroup(group)
          group.students[student.id] = student
        } else {
          await student.removeFromGroup(group)
          await group.removeStudent(student)
        }
      }
    }
    return dom
  },
  setQuestionFontSize (): void {
    const questionsContainer = document.querySelector('#questions-list > .container') as HTMLElement
    const oneQuestion = document.querySelector('#questions-list > .container .question') as HTMLElement
    if (questionsContainer !== null && oneQuestion !== null) {
      const width = oneQuestion.getBoundingClientRect().width
      const pixelSize = width / textToQuestionWidthFactor
      questionsContainer.style.fontSize = String(pixelSize) + 'px'
      utils.image.defaultPixelSize = pixelSize
    }
  },
  setQCMQuestionFontSize (): void {
    let questionsContainer = document.querySelector('#qcm-container') as HTMLElement
    if (questionsContainer !== null) {
      const boundingBox = questionsContainer.getBoundingClientRect()
      const width = Math.min(boundingBox.width - 40, (boundingBox.height - 60) * 4 / 3)
      questionsContainer.style.fontSize = String(width / textToQuestionWidthFactor) + 'px'
    }
    questionsContainer = document.querySelector('#qcm-container0') as HTMLElement
    if (questionsContainer !== null) {
      const boundingBox = questionsContainer.getBoundingClientRect()
      const width = Math.min(boundingBox.width - 40, (boundingBox.height - 60) * 4 / 3)
      questionsContainer.style.fontSize = String(width / textToQuestionWidthFactor) + 'px'
    }
  },
  setQCMCardsFontSize (): void {
    const questionContainers = document.querySelectorAll<HTMLElement>('#qcms-list .question-container')
    for (const questionContainer of Array.from(questionContainers)) {
      const width = questionContainer.getBoundingClientRect().width
      const pixelSize = width / textToQuestionWidthFactor
      questionContainer.style.fontSize = String(pixelSize) + 'px'
      utils.image.defaultPixelSize = pixelSize
    }
  },
  createQcmCard (qcms: QcmList, index: number, question: Question) {
    const id = qcms.list[index][0]
    const title = qcms.list[index][1]
    const article = utils.DOM.create('article', { class: 'card m-4' })
    const divMedia = utils.DOM.create('div', { class: 'media mb-4' })
    const divMediaLeft = utils.DOM.create('div', { class: 'media-left' })
    const iconMedia = utils.DOM.create('i', { class: 'ri-file-line title is-3' })
    divMediaLeft.appendChild(iconMedia)
    divMedia.appendChild(divMediaLeft)
    const divMediaContent = utils.DOM.create('div', { class: 'media-content' })
    const paragraphTitle = utils.DOM.create('p', { class: 'title', text: title })
    divMediaContent.appendChild(paragraphTitle)
    // const paragraphSubtitle = utils.DOM.create('p', { class: 'subtitle', text: 'mots clé' })
    // divMediaContent.appendChild(paragraphSubtitle)
    divMedia.appendChild(divMediaContent)
    article.appendChild(divMedia)
    const divContent = utils.DOM.create('div', { class: 'card-content p-4' })
    const divQuestionContainer = utils.DOM.create('div', { class: 'question-container' })
    divContent.appendChild(divQuestionContainer)
    const divQuestion = utils.DOM.create('div', { class: 'question' })
    if (question !== undefined) {
      if (!isNaN(question.textSize)) divQuestion.style.fontSize = String(question.textSize) + 'em'
      divQuestion.classList.add(question.displayMode)
      const questionHeader = utils.DOM.create('header', { class: 'question-content ' + question.type })
      const $divHeader = utils.DOM.create('div')
      $divHeader.innerHTML = question.content
      questionHeader.appendChild($divHeader)
      const answersContainer = utils.DOM.create('ol', { class: 'answers ' + question.type }) as HTMLOListElement
      const answersList = [...question.answers]
      if (question.shuffleMode) utils.array.shuffle(answersList)
      for (const answer of answersList) {
        const answerContent = utils.DOM.create('li', { class: 'answer ' + question.type })
        const $divAnswer = utils.DOM.create('div')
        $divAnswer.innerHTML = answer.content
        answerContent.appendChild($divAnswer)
        if (answer.isCorrect) { answerContent.classList.add('is-correct') }
        answersContainer.appendChild(answerContent)
      }
      divQuestion.appendChild(questionHeader)
      divQuestion.appendChild(answersContainer)
    }
    divQuestionContainer.appendChild(divQuestion)
    article.appendChild(divContent)
    const footer = utils.DOM.create('footer', { class: 'card-footer' })
    const footerEdit = utils.DOM.createAnchor({ class: 'card-footer-item' })
    footerEdit.innerHTML = '<i class="ri-edit-line"></i>'
    footerEdit.title = 'Modifier'
    footerEdit.onclick = () => {
      Qcm.load(id).then(e => {
        storage.db.store_data.put({ uid: 'QCMCamlatestSelectedQCM', data: id })
        display.QCMQuestionsList(title, e, qcms, index)
      })
        .catch((e) => {
          alert('Erreur lors du chargement du QCM')
          console.warn(e)
          // if (qcms.removeQcmReference(index)) this.QCMList(qcms)
        })
    }
    footer.appendChild(footerEdit)
    const footerCopy = utils.DOM.createAnchor({ class: 'card-footer-item' })
    footerCopy.innerHTML = '<i class="ri-file-copy-line"></i>'
    footerCopy.title = 'Dupliquer'
    footerCopy.onclick = () => {
      Qcm.load(id).then(qcm => {
        qcm.copy().then(newQcm => {
          newQcm.save()
          qcms.list.push([newQcm.id, newQcm.name, newQcm.questionsList[0], newQcm.questionsList.length])
          qcms.save()
          this.QCMList(qcms)
        }).catch(err => { console.error('Errur de copie', err) })
      })
        .catch(() => { return false })
    }
    footer.appendChild(footerCopy)
    const footerDownload = utils.DOM.createAnchor({ class: 'card-footer-item' })
    footerDownload.innerHTML = '<i class="ri-file-download-line"></i>'
    footerDownload.title = 'Exporter'
    footerDownload.onclick = async () => {
      const qcm = await Qcm.load(id)
      const content = await qcm.export()
      const blob = new Blob([JSON.stringify(content, null, 2)], { type: 'application/json' })
      try {
        utils.file.download(blob, qcm.name + '.txt')
      } catch (err) {
        console.warn('oupsi export qcm !', err)
      }
    }
    footer.appendChild(footerDownload)
    const footerDelete = utils.DOM.createAnchor({ class: 'card-footer-item' })
    footerDelete.innerHTML = '<i class="ri-delete-bin-line"></i>'
    footerDelete.title = 'Supprimer'
    footerDelete.onclick = async () => {
      if (confirm('Vous allez supprimer le qcm \n' + title + '\nConfirmez-vous ?')) {
        await qcms.deleteQcmById(id)
        qcms.save()
        this.QCMList(qcms)
      }
    }
    footer.appendChild(footerDelete)
    article.appendChild(footer)
    return article
  },
  showQRCode: function (peerId: string): void {
    // do nothing now
  },
  peerConnected: function () {
    utils.DOM.addClass('btnportable', 'is-hidden')
    utils.DOM.removeClass('btnstopportable', 'is-hidden')
  },
  connectedMessage: function () {
    utils.DOM.text('cameras-list', language.dictionary.messages.connexionestablished)
    utils.DOM.removeClass('qrious', 'montre')
    utils.DOM.addClass('qrious', 'cache')
    // on enlève le cache gris des question si présent
    // diaporama.toggleCacheQuestions(true)
  }
}

export { display as default }
